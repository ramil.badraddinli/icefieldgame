package items;

import figures.Figure;

/**
 * Represents Food item
 */
public class Food implements Item{

    /** Performs the usage of diving food item
     *
     * @param object represent figure object who will eat food
     * @return true, if body heat is increased, otherwise false
     */
    @Override
    public boolean useItem(Object object) {
        Figure figure = (Figure) object;
        int temp = figure.getBodyHeat();
        figure.increaseBodyHeat(1);
        return temp==figure.getBodyHeat();
    }
}
