package items;

import elements.Iceberg;
import figures.Figure;

/**
 * Represents diving suit item
 */
public class DivingSuit implements Item{
    /**
     * A Figure object representing the figure to which the diving suit belongs
     */
    private Figure belongsTo;

    /** Parametrized constructor
     *
     * @param belongsTo A Figure object to which belongsTo field is set
     */
    public DivingSuit(Figure belongsTo) {
        this.belongsTo = belongsTo;
    }

    /**
     * Default constructor
     */
    public DivingSuit() {
    }

    /** Gets the figure object  to which diving suit belongs
     *
     * @return A Figure object representing figure that diving suit belongs to
     */
    public Figure getBelongsTo() {
        return belongsTo;
    }

    /** Sets the belongsTo field
     *
     * @param belongsTo A Figure object that is to be set for belongsTo field
     */
    public void setBelongsTo(Figure belongsTo) {
        this.belongsTo = belongsTo;
    }

    /** Performs the usage of diving suit item
     *
     * @param object iceberg object
     * @return true if figure has used the diving suit correctly and got on an iceberg , false if not
     */
    @Override
    public boolean useItem(Object object) {
        Iceberg iceberg = (Iceberg) object;
        belongsTo.setOnIceberg(iceberg);
        iceberg.addFigure(belongsTo);
        return iceberg.getFigures().contains(belongsTo);
    }
}
