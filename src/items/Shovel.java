package items;

import elements.Iceberg;

/**
 * Represents Shovel item
 */
public class Shovel implements Item{

    /** Performs the usage of diving shovel item
     *
     * @param object represents figure object who will use shovel item
     * @return true if snow was removed from iceberg, otherwise false
     */
    @Override
    public boolean useItem(Object object) {
        Iceberg iceberg = (Iceberg) object;
        iceberg.removeSnow(2);
        return true;
    }
}
