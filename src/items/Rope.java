package items;

import elements.Iceberg;
import figures.Figure;

/**
 * Represents Rope item
 */
public class Rope implements Item{

    /**
     * represents figure object who is rescued by rope item
     */
    private Figure savedFigure;

    /** Constructor
     *
     * @param savedFigure figure object which will be rescued
     */
    public Rope(Figure savedFigure) {
        this.savedFigure = savedFigure;
    }

    public Rope() {
    }

    /** Gets the saved figure
     *
     * @return saved figure object
     */
    public Figure getSavedFigure() {
        return savedFigure;
    }

    /** Sets savedFigure field
     *
     * @param savedFigure represents object which is to be saved
     */
    public void setSavedFigure(Figure savedFigure) {
        this.savedFigure = savedFigure;
    }

    /** Performs the usage of rope item
     *
     * @param object represents object types which is depend on usage of this function
     * @return true, if figure is saved, otherwise false
     */
    @Override
    public boolean useItem(Object object) {
        Iceberg iceberg = (Iceberg) object;
        if(savedFigure.isFallen()==false) return false;
        savedFigure.setOnIceberg(iceberg);
        iceberg.addFigure(savedFigure);
        return iceberg.getFigures().contains(savedFigure);
    }
}
