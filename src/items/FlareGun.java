package items;

/**
 * Represents flare gun item
 */
public class FlareGun implements Item {
    /**
     * An integer number representing the number of parts of flare gun that have been collected
     */
    private int collectedParts;

    /** Parametrized constructor which initializes collectedParts field
     *
     * @param collectedParts An integer number that is to be set for collectedParts field
     */
    public FlareGun(int collectedParts) {
        this.collectedParts = collectedParts;
    }

    /**
     * Constructor
     */
    public FlareGun() {
        this.collectedParts = 0;
    }

    /** Gets collectedParts field
     *
     * @return An integer which is value of collectedParts field
     */
    public int getCollectedParts() {
        return collectedParts;
    }

    /** Sets the collectedParts field
     *
     * @param collectedParts An integer number that is to be set for colllectedParts field
     */
    public void setCollectedParts(int collectedParts) {
        this.collectedParts = collectedParts;
    }

    /**
     * Increments collectedParts field by one
     */
    public void addPart() {
        this.collectedParts++;
    }

    /** Performs the usage of flare gun item
     *
     * @param object An object
     * @return true if all three parts of flare gun have been collected , if not false
     */
    @Override
    public boolean useItem(Object object) {
        if(collectedParts!=3) return false;
        return true; // fire flare gun and win
    }
}
