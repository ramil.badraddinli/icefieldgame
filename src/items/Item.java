package items;

/**
 * Interface for all items
 */
public interface Item {
    /** Represents interface for all times which will be used during the game
     *
     * @param object represents object types which is depend on usage of this function
     * @return returns true or false depend on item usage
     */
    public boolean useItem(Object object);
}
