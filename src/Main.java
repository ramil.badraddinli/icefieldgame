import gui.WelcomeView;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        WelcomeView welcomeView = new WelcomeView();
        welcomeView.instantiateFrame();
    }
}
