package elements;

import java.util.ArrayList;

/**
 * Represents the blizzard event during the game
 */
public class Blizzard {
    /**
     * Represents if blizzard is active on certain iceberg or not
     */
    boolean isActive;

    /**
     * Constructor
     */
    public Blizzard() {
        isActive = false;
    }

    /**
     * Disables the blizzard event by setting isActive param to false
     */
    public void disable() {
        isActive = false;
    }

    /**
     * Activates the blizzard event by setting isActive param to false
     */
    public void activate() {
        isActive = true;
    }

    /** Kills the figures who are not in an igloo during a blizzard attack
     *
     * @param iceberg Iceberg object representing which iceberg the blizzard is happening on
     * @return true if there is no igloo on iceberg and all figures are killed
     *         false if there is an igloo on iceberg
     */
    public boolean tumbleFigures(Iceberg iceberg) {
        ArrayList<Iceberg> neighbours = iceberg.getNeighbours();
        if (iceberg.isHasIgloo()) return false;
        // figures won't be tumbled because there is igloo on this iceberg
        iceberg.getNeighbourHoles().get(0).fallFigures(iceberg.getFigures());
        return iceberg.removeAllFigures();
    }
}
