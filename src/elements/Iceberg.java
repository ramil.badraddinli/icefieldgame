package elements;

import figures.Eskimos;
import figures.Explorer;
import figures.Figure;
import items.*;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Represents Iceberg class
 */
public class Iceberg {
    /**
     * represents capacity of iceberg that is how many figures can stand on it
     */
    private int capacity;

    /**
     * represents if the iceberg is stable or not that is it can take at least 1 more figure
     */
    private boolean isStable;

    /**
     * represents how much snow exists on iceberg
     */
    private int amountOfSnow;

    /**
     * Represents the list of figures existing on iceberg
     */
    private ArrayList<Figure> figures;

    /**
     * Represents the list of eskimo type figures on iceberg
     */
    private ArrayList<Eskimos> eskimos;

    /**
     * Represents the list of explorer type figures on iceberg
     */
    private ArrayList<Explorer> explorers;

    /**
     * Represents the list of holes these are around this iceberg
     */
    private ArrayList<Hole> neighbourHoles;

    /**
     * Integer ID identifies iceberg
     */
    private int ID;

    /**
     * List representing the items on iceberg
     */
    private ArrayList<Item> items;

    /**
     * A bool variable indicating whether there is an igloo on iceberg or not
     */
    private boolean hasIgloo;

    /**
     * List representing neighbors of an iceberg
     */
    private ArrayList<Iceberg> neighbours;
    private ArrayList<Food> foods;
    private ArrayList<DivingSuit> divingSuits;
    private ArrayList<Rope> ropes;
    private ArrayList<Shovel> shovels;
    private ArrayList<FlareGun> flareGuns;
    private ArrayList<JLabel> labels;

    /**
     * Constructor
     * Initializing iceberg objects
     */
    public Iceberg() {
        this.figures = new ArrayList<Figure>();
        this.eskimos = new ArrayList<Eskimos>();
        this.explorers = new ArrayList<Explorer>();
        this.items = new ArrayList<Item>();
        this.neighbours = new ArrayList<Iceberg>();
        this.foods = new ArrayList<Food>();
        this.divingSuits = new ArrayList<DivingSuit>();
        this.ropes = new ArrayList<Rope>();
        this.shovels = new ArrayList<Shovel>();
        this.flareGuns = new ArrayList<FlareGun>();
        this.labels = new ArrayList<JLabel>();
    }

    public ArrayList<Hole> getNeighbourHoles() {
        return neighbourHoles;
    }

    /**
     * Checks whether iceberg has an igloo or not
     *
     * @return true if there is igloo, otherwise false
     */
    public boolean isHasIgloo() {
        return hasIgloo;
    }

    /**
     * Updates hasIgloo field representing if an iceberg has igloo or not
     *
     * @param hasIgloo boolean variable indicating if iceberg has an igloo or not
     */
    public void setHasIgloo(boolean hasIgloo) {
        this.hasIgloo = hasIgloo;
    }

    /**
     * Gets the capacity of iceberg
     *
     * @return the capacity of iceberg
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Sets the capacity of iceberg
     *
     * @param capacity integer number representing capacity of iceberg
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * Checks whether iceberg is stable or not
     *
     * @return true if the iceberg is stable, otherwise false
     */
    public boolean isStable() {
        return isStable;
    }

    /**
     * Updates the info about the stability of iceberg
     *
     * @param stable boolean variable representing status of stability
     */
    public void setStable(boolean stable) {
        isStable = stable;
    }

    /**
     * Gets the amount of snow on iceberg
     *
     * @return amount of snow existing on iceberg
     */
    public int getAmountOfSnow() {
        return amountOfSnow;
    }

    /**
     * Sets the amount of snow on iceberg
     *
     * @param amountOfSnow integer represents how much snow is to be set for iceberg
     */
    public void setAmountOfSnow(int amountOfSnow) {
        this.amountOfSnow = amountOfSnow;
    }

    /**
     * Gets the ID of particular iceberg
     *
     * @return integer representing ID of iceberg
     */
    public int getID() {
        return ID;
    }

    /**
     * Sets an ID for iceberg
     *
     * @param ID integer which is to be set for ID field
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * Returns the list of items existing on iceberg
     *
     * @return a list of Item objects representing the items existing on iceberg
     */
    public ArrayList<Item> getItems() {
        return items;
    }

    /**
     * Sets the list of items existing on iceberg
     *
     * @param items a list of Item objects which is to be set for the items list
     */
    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    /**
     * Decrements the value of amountsOfSnow field by the amount of snow that is removed from iceberg
     *
     * @param unit an integer number representing the amount of snow that is removed from iceberg
     */
    public void removeSnow(int unit) {
        amountOfSnow -= unit;
        amountOfSnow = Math.max(0,amountOfSnow);
    }

    /**
     * Extends the figures list with either an Eskimos or an Explorer object
     *
     * @param figure A figure object which is to be added to figures list that is to be placed on iceberg
     */
    public void addFigure(Figure figure) {
        if (figure instanceof Eskimos) {
            eskimos.add((Eskimos) figure);
        } else if (figure instanceof Explorer) {
            explorers.add((Explorer) figure);
        }
        figures.add(figure);
    }

    /**
     * Extends the figures and eskimos lists with an Eskimos object
     *
     * @param eskimos An Eskimos object which is to be added to figures and eskimos lists
     */
    public void addEskimos(Eskimos eskimos) {
        this.eskimos.add(eskimos);
        this.figures.add(eskimos);
    }

    /**
     * Returns the list of Eskimos objects existing on iceberg
     *
     * @return A list of Eskimos objects representing eskimos standing on iceberg
     */
    public ArrayList<Eskimos> getEskimos() {
        return this.eskimos;
    }

    /**
     * Extends the figures and explorers lists with an Explorer object
     *
     * @param explorer An Explorer object which is to be added to figures and explorers lists
     */
    public void addExplorer(Explorer explorer) {
        this.explorers.add(explorer);
        this.figures.add(explorer);
    }

    /**
     * Returns the list of Explorer objects existing on iceberg
     *
     * @return A list of Explorer objects representing explorers standing on iceberg
     */
    public ArrayList<Explorer> getExplorers() {
        return this.explorers;
    }

    /**
     * Returns the list of Figure objects existing on iceberg
     *
     * @return A list of Figure objects representing figures standing on iceberg
     */
    public ArrayList<Figure> getFigures() {
        return this.figures;
    }

    /**
     * Removes a Figure object from figures and eskimos or explorers lists, depending on which type of figure is removed
     *
     * @param figure A Figure object which to be removed from iceberg
     */
    public void removeFigure(Figure figure) {
        if (figure instanceof Eskimos) {
            eskimos.remove((Eskimos) figure);
            figures.remove(figure);
        } else if (figure instanceof Explorer) {
            explorers.remove((Explorer) figure);
            figures.remove(figure);
        }
    }

    /**
     * Places a neighbor iceberg around the iceberg
     *
     * @param iceberg An Iceberg object representing the neighbor iceberg
     */
    public void addNeighbour(Iceberg iceberg) {
        this.neighbours.add(iceberg);
    }

    /**
     * Sets the list of figures existing on iceberg
     *
     * @param figures A list of Figure objects that are to be set for figures list
     */
    public void setFigures(ArrayList<Figure> figures) {
        this.figures = figures;
    }

    /**
     * Sets the list of eskimos existing on iceberg
     *
     * @param eskimos A list of Eskimos objects that are to be set for eskimos list
     */
    public void setEskimos(ArrayList<Eskimos> eskimos) {
        this.eskimos = eskimos;
    }

    /**
     * Sets the list of explorers existing on iceberg
     *
     * @param explorers A list of Explorer objects that are to be set for explorers list
     */
    public void setExplorers(ArrayList<Explorer> explorers) {
        this.explorers = explorers;
    }

    /**
     * Sets the list of neighbor icebergs surrounding the iceberg
     *
     * @param neighbours A list of Iceberg objects that are to be set for neighbours list
     */
    public void setNeighbours(ArrayList<Iceberg> neighbours) {
        this.neighbours = neighbours;
    }

    /**
     * Returns the list of icebergs surrounding the iceberg
     *
     * @return A list of Iceberg objects representing icebergs surrounding the iceberg
     */
    public ArrayList<Iceberg> getNeighbours() {
        return this.neighbours;
    }

    public void addFood (Food food) {
        this.foods.add(food);
        this.items.add(food);
    }

    public ArrayList<Food> getFoods() {
        return this.foods;
    }

    public void addDivingSuit (DivingSuit divingSuit) {
        this.divingSuits.add(divingSuit);
        this.items.add(divingSuit);
    }

    public ArrayList<DivingSuit> getDivingSuits() {
        return this.divingSuits;
    }

    public void addRope (Rope rope) {
        this.ropes.add(rope);
        this.items.add(rope);
    }

    public ArrayList<Rope> getRopes() {
        return this.ropes;
    }

    public void addShovel (Shovel shovel) {
        this.shovels.add(shovel);
        this.items.add(shovel);
    }

    public ArrayList<Shovel> getShovels() {
        return this.shovels;
    }

    public boolean removeAllFigures() {
        figures.clear();
        explorers.clear();
        eskimos.clear();
        return (figures.size() == 0 && eskimos.size() == 0 && explorers.size() == 0);
    }

    public void addLabel (JLabel label) {
        this.labels.add(label);
    }

    public ArrayList<JLabel> getLabels() {
        return this.labels;
    }

    public void addFlareGun (FlareGun flareGun) {
        this.flareGuns.add(flareGun);
        this.items.add(flareGun);
    }

    public ArrayList<FlareGun> getFlareGuns() {
        return this.flareGuns;
    }
}
