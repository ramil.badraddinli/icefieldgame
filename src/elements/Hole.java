package elements;

import figures.Figure;

import java.util.ArrayList;

/**
 * Represents Hole which is water surrounding icebergs
 */
public class Hole {

    /**
     * Represents fallen figures to hole
     */
    private ArrayList<Figure> fallenFigures;
    /**
     * Constructor
     */
    public Hole() {
        fallenFigures = new ArrayList<>();
    }

    /** Return boolean value represents whether fallen figures were added to the list correctly or not
     *
     * @param figures represents the list of figures which have fallen into hole
     * @return void
     */
    public void fallFigures(ArrayList<Figure> figures) {
        for (Figure figure : figures
        ) {
            fallenFigures.add(figure);
        }
    }
}
