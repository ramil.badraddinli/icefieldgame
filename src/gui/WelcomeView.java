package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class WelcomeView {
    private JFrame fMain;
    private JLabel lWelcome;
    private JButton bStart;
    private JButton bLeaderBoard;
    private JLabel lDifficulty;
    private JRadioButton rbEasy;
    private JRadioButton rbMedium;
    private JRadioButton rbHard;
    private ButtonGroup buttonGroup;
    private InputFrame inputFrame;

    public WelcomeView() {
        fMain = new JFrame("Welcome to Icefield");
        lWelcome = new JLabel("Welcome to IceField game.Game!!");
        bStart = new JButton("Start Game");
        bLeaderBoard = new JButton("LeaderBoard");
        lDifficulty = new JLabel("Choose Difficulty:");
        rbEasy = new JRadioButton("Easy");
        rbMedium = new JRadioButton("Medium");
        rbHard = new JRadioButton("Hard");
        buttonGroup = new ButtonGroup();
        buttonGroup.add(rbEasy);
        buttonGroup.add(rbMedium);
        buttonGroup.add(rbHard);
        this.inputFrame = new InputFrame("Easy");
    }

    public void instantiateFrame(){
        fMain.setSize(600,300);
        fMain.getContentPane().setBackground(Color.DARK_GRAY);
        fMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        lWelcome.setForeground(Color.yellow.brighter());
        lWelcome.setFont (new Font ("Serif",Font.ITALIC, 34));
        lWelcome.setBounds(20,0,550,110);

        lDifficulty.setForeground(Color.green.brighter());
        lDifficulty.setFont (new Font ("Times New Roman",Font.ITALIC, 18));
        lDifficulty.setBounds(20,130,180,40);

        rbEasy.setBounds(200,130,100,40);
        rbEasy.setBackground(Color.darkGray);
        rbEasy.setForeground(Color.green.brighter());
        rbEasy.setFont (new Font ("Times New Roman",Font.ITALIC, 18));
        rbEasy.setActionCommand("Easy");

        rbMedium.setBounds(300,130,100,40);
        rbMedium.setBackground(Color.darkGray);
        rbMedium.setForeground(Color.green.brighter());
        rbMedium.setFont (new Font ("Times New Roman",Font.ITALIC, 18));
        rbMedium.setActionCommand("Medium");

        rbHard.setBounds(400,130,100,40);
        rbHard.setBackground(Color.darkGray);
        rbHard.setForeground(Color.green.brighter());
        rbHard.setFont (new Font ("Times New Roman",Font.ITALIC, 18));
        rbMedium.setActionCommand("Hard");

        bLeaderBoard.setBounds(20,200,250,50);
        bLeaderBoard.setFont(new Font ("Times New Roman",Font.ITALIC, 16));
        bLeaderBoard.setBackground(Color.pink);
        bLeaderBoard.setForeground(Color.darkGray);

        bStart.setBounds(300,200,250,50);
        bStart.setFont(new Font ("Times New Roman",Font.ITALIC, 16));
        bStart.setBackground(Color.pink);
        bStart.setForeground(Color.darkGray);

        addActionListeners();

        fMain.add(bStart);
        fMain.add(bLeaderBoard);
        fMain.add(rbEasy);
        fMain.add(rbMedium);
        fMain.add(rbHard);
        fMain.add(lDifficulty);
        fMain.add(lWelcome);

        fMain.setLayout(null);
        fMain.setLocationRelativeTo(null);
        fMain.setVisible(true);
    }

    public void addActionListeners(){
        bStart.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String level =  "";
                level = buttonGroup.getSelection().getActionCommand();
                inputFrame.instantiateFrame();
                fMain.setVisible(false); //you can't see me!
                fMain.dispose();  }
        });

        bLeaderBoard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LeaderBoardView leaderBoardView = new LeaderBoardView();
                try {
                    leaderBoardView.instantiateFrame();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                fMain.setVisible(false);
            }
        });

    }

    public JFrame getfMain() {
        return fMain;
    }

    public JLabel getlWelcome() {
        return lWelcome;
    }

    public JButton getbStart() {
        return bStart;
    }

    public JButton getbLeaderBoard() {
        return bLeaderBoard;
    }

    public JLabel getlDifficulty() {
        return lDifficulty;
    }

    public JRadioButton getRbEasy() {
        return rbEasy;
    }

    public JRadioButton getRbMedium() {
        return rbMedium;
    }

    public JRadioButton getRbHard() {
        return rbHard;
    }

    public ButtonGroup getButtonGroup() {
        return buttonGroup;
    }

    public InputFrame getInputFrame() {
        return this.inputFrame;
    }
}
