package gui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class MoveView {
    private Parameter parameter;
    private JPanel pMoves;
    private JButton bRight;
    private JButton bLeft;
    private JButton bUp;
    private JButton bDown;
    private JButton bRemoveSnow;
    private JButton bRetrieveItem;
    private ArrayList<ImageIcon> icons;

    public MoveView(Parameter parameter) {
        pMoves = new JPanel();
        icons = new ArrayList<>();
        icons.add(new ImageIcon("icons/down.png"));
        icons.add(new ImageIcon("icons/up.png"));
        icons.add(new ImageIcon("icons/left.png"));
        icons.add(new ImageIcon("icons/right.png"));
        bDown = new JButton(icons.get(0));
        bUp = new JButton(icons.get(1));
        bLeft = new JButton(icons.get(2));
        bRight = new JButton(icons.get(3));
        bRemoveSnow = new JButton();
        bRetrieveItem = new JButton();
        this.parameter = parameter;
    }

    public void locateElements() {
        pMoves.setBackground(null);
        pMoves.setBounds(430, 0, 270, 400);
        pMoves.setLayout(null);

        bRetrieveItem.setText("Retrieve Item");
        bRemoveSnow.setText("Remove Snow");

        bRemoveSnow.setForeground(Color.LIGHT_GRAY);
        bRetrieveItem.setForeground(Color.LIGHT_GRAY);
        bLeft.setForeground(Color.LIGHT_GRAY);
        bRight.setForeground(Color.LIGHT_GRAY);
        bUp.setForeground(Color.LIGHT_GRAY);
        bDown.setForeground(Color.LIGHT_GRAY);

        bRemoveSnow.setBackground(Color.BLACK);
        bRetrieveItem.setBackground(Color.BLACK);
        bLeft.setBackground(null);
        bRight.setBackground(null);
        bUp.setBackground(null);
        bDown.setBackground(null);

        bRetrieveItem.setBounds(20, 20, 220, 40);
        bRemoveSnow.setBounds(20, 80, 220, 40);
        bLeft.setBounds(20, 200, 70, 60);
        bRight.setBounds(170, 200, 70, 60);
        bUp.setBounds(100, 160, 60, 70);
        bDown.setBounds(100, 240, 60, 70);

        bRetrieveItem.setName("Retrieve");
        bRemoveSnow.setName("Remove");
        bLeft.setName("Left");
        bRight.setName("Right");
        bUp.setName("Up");
        bDown.setName("Down");

        pMoves.add(bRetrieveItem);
        pMoves.add(bRemoveSnow);
        pMoves.add(bLeft);
        pMoves.add(bRight);
        pMoves.add(bUp);
        pMoves.add(bDown);
    }

    public void addListeners () {
        bRetrieveItem.addActionListener(new MoveListener(parameter, bRetrieveItem));
        bRemoveSnow.addActionListener(new MoveListener(parameter, bRemoveSnow));
        bLeft.addActionListener(new MoveListener(parameter, bLeft));
        bRight.addActionListener(new MoveListener(parameter, bRight));
        bUp.addActionListener(new MoveListener(parameter, bUp));
        bDown.addActionListener(new MoveListener(parameter, bDown));
    }

    public JPanel getpMoves() {
        return pMoves;
    }

    public void setpMoves(JPanel pMoves) {
        this.pMoves = pMoves;
    }

    public JButton getbRight() {
        return bRight;
    }

    public void setbRight(JButton bRight) {
        this.bRight = bRight;
    }

    public JButton getbLeft() {
        return bLeft;
    }

    public void setbLeft(JButton bLeft) {
        this.bLeft = bLeft;
    }

    public JButton getbUp() {
        return bUp;
    }

    public void setbUp(JButton bUp) {
        this.bUp = bUp;
    }

    public JButton getbDown() {
        return bDown;
    }

    public void setbDown(JButton bDown) {
        this.bDown = bDown;
    }

    public JButton getbRemoveSnow() {
        return bRemoveSnow;
    }

    public void setbRemoveSnow(JButton bRemoveSnow) {
        this.bRemoveSnow = bRemoveSnow;
    }

    public JButton getbRetrieveItem() {
        return bRetrieveItem;
    }

    public void setbRetrieveItem(JButton bRetrieveItem) {
        this.bRetrieveItem = bRetrieveItem;
    }
}
