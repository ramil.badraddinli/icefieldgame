package gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FieldView {
    // Attributes
    private JFrame frame;
    private Parameter parameter;
    private IcebergView icebergView;
    private EskimosView eskimosView;
    private ExplorerView explorerView;
    private FigureListener figureListener;

    public FieldView(Parameter parameter) {
        this.frame = new JFrame();
        this.icebergView = new IcebergView();
        this.parameter = parameter;
        this.eskimosView = new EskimosView();
        this.explorerView = new ExplorerView();
    }

    public JFrame getFrame() {
        return frame;
    }

    // Methods
    public void draw() throws IOException {
        this.frame.setTitle("Icefield");
        this.parameter.setFrame(this.frame);
        BufferedImage image = ImageIO.read(new File("src/files/sea_background.jpg"));
        this.frame.setContentPane(new ImagePanel(image));
        icebergView.draw(parameter.getNumber_of_icebergs(), frame, this.parameter);
        eskimosView.draw(parameter, icebergView.getIcebergs());
        explorerView.draw(parameter, icebergView.getIcebergs());
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setSize(700, 700);
        this.frame.setLayout(null);
        this.frame.setLocationRelativeTo(null);
        this.frame.setVisible(true);
        System.out.println("I am here");
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    public Parameter getParameter() {
        return this.parameter;
    }
}
