package gui;

import elements.Iceberg;
import figures.Eskimos;
import figures.Explorer;
import figures.Figure;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SkillView {
    private ControllerFrame controllerFrame;
    private String message;
    private Parameter parameter;
    private JLabel lSkill;
    private JPanel pSkills;
    private JButton bIgloo;
    private JButton bCapacity;

    public SkillView(Parameter parameter, ControllerFrame controllerFrame) {
        this.parameter = parameter;
        this.controllerFrame = controllerFrame;
        this.lSkill = new JLabel("Apply Skill");
        pSkills = new JPanel();
        bIgloo = new JButton();
        bCapacity = new JButton();
    }

    public void locateElements() throws IOException {
        pSkills.setBounds(0, 0, 190, 200);
        pSkills.setBackground(null);
        pSkills.setLayout(null);

        lSkill.setForeground(Color.green.brighter());
        lSkill.setFont (new Font ("Times New Roman",Font.ITALIC, 25));
        lSkill.setBounds(10,10,180,60);

        bIgloo.setBounds(10, 70, 75, 75);
        bCapacity.setBounds(100, 70, 75, 75);

        bIgloo.setBackground(Color.BLACK);
        bCapacity.setBackground(Color.WHITE);

        bCapacity.setForeground(Color.lightGray);
        bIgloo.setForeground(Color.lightGray);


        BufferedImage bufferedImage = ImageIO.read(new File("icons/igloo.jpg"));
        Image image = bufferedImage.getScaledInstance(75, 75, Image.SCALE_SMOOTH);
        bIgloo.setIcon(new ImageIcon(image));

        bufferedImage = ImageIO.read(new File("icons/capacity.png"));
        image = bufferedImage.getScaledInstance(75, 75, Image.SCALE_SMOOTH);
        bCapacity.setIcon(new ImageIcon(image));

        addListeners();

        pSkills.add(lSkill);
        pSkills.add(bCapacity);
        pSkills.add(bIgloo);
    }

    private void addListeners() {
        bIgloo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int icebergID = Integer.valueOf(parameter.getSelectedLabel().getName());
                int figureIndex = Integer.valueOf(parameter.getSelectedLabel().getToolTipText());
                Figure figure = parameter.getIcebergByID(icebergID).getFigures().
                        get(figureIndex);
                if(figure instanceof Eskimos){
                    JLabel label = parameter.getSelectedLabel();
                    int icebergId = Integer.parseInt(label.getName()) - 1;
                    JPanel icebergPanel = parameter.getIcebergPanels().get(icebergId);
                    Eskimos eskimos = (Eskimos) figure;
                    eskimos.buildIgloo(parameter.getIcebergByID(icebergID));
                    controllerFrame.getlInfo().setText("Igloo was built!!");
                    JLabel igloo = new JLabel();
                    BufferedImage image = null;
                    try {
                        image = ImageIO.read(new File("icons/igloo.jpg"));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    Image dimg = image.getScaledInstance(35, 35, Image.SCALE_SMOOTH);
                    igloo.setIcon(new ImageIcon(dimg));
                    Iceberg iceberg = parameter.getIcebergs().get(icebergId);
                    iceberg.addLabel(igloo);
                    icebergPanel.add(igloo);
                    locateFiguresOnIceberg(iceberg.getLabels());
                    icebergPanel.revalidate();
                    icebergPanel.repaint();
                }
                if(figure instanceof Explorer){
                    controllerFrame.getlInfo().setText("Igloo was  NOT built!!");
                    controllerFrame.getlError().setText("Explorer cannot build Igloo!!");
                }
                writeInformationMessage(parameter.getMoveCount(), parameter.getControllerFrame().getlInfo(), parameter);
                parameter.increaseMoveCount();
            }
        });
        bCapacity.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int icebergID = Integer.valueOf(parameter.getSelectedLabel().getName());
                int figureIndex = Integer.valueOf(parameter.getSelectedLabel().getToolTipText());
                Figure figure = parameter.getIcebergByID(icebergID).getFigures().
                        get(figureIndex);
                if(figure instanceof Eskimos){
                    controllerFrame.getlInfo().setText("Capacity is unknown!!");
                    controllerFrame.getlInfo2().setText("Eskimos cannot see capacity!!");
                }
                if(figure instanceof Explorer){
                    controllerFrame.getlInfo().setText("Iceberg Capacity: "
                            + parameter.getIcebergByID(icebergID).getCapacity());
                }
                writeInformationMessage(parameter.getMoveCount(), parameter.getControllerFrame().getlInfo(), parameter);
                parameter.increaseMoveCount();
            }
        });
    }

    public void writeInformationMessage(int moveCount, JLabel textField, Parameter parameter) {
        if (moveCount <= 20) {
            if (moveCount >= 1 && moveCount <= 4) {
                textField.setText("Player: Player1" + " Move: " +Integer.toString(moveCount) + " Round: 1");
            } else if (moveCount >= 5 && moveCount <= 8) {
                textField.setText("Player: Player2" + " Move: " +Integer.toString(moveCount - 4) + " Round: 1");
            } else if (moveCount >= 9 && moveCount <= 12) {
                textField.setText("Player: Player3" + " Move: " +Integer.toString(moveCount - 8) + " Round: 1");
            } else if (moveCount >= 13 && moveCount <= 16) {
                textField.setText("Player: Player4" + " Move: " +Integer.toString(moveCount - 12) + " Round: 1");
            } else if (moveCount >= 17 && moveCount <= 20) {
                textField.setText("Player: Player5" + " Move: " +Integer.toString(moveCount - 16) + " Round: 1");
            }
        } else if (moveCount > 20 && moveCount <= 40) {
            if (moveCount >= 21 && moveCount <= 24) {
                textField.setText("Player: Player1" + " Move: " +Integer.toString(moveCount - 20) + " Round: 2");
            } else if (moveCount >= 25 && moveCount <= 28) {
                textField.setText("Player: Player2" + " Move: " +Integer.toString(moveCount - 24) + " Round: 2");
            } else if (moveCount >= 29 && moveCount <= 32) {
                textField.setText("Player: Player3" + " Move: " +Integer.toString(moveCount - 28) + " Round: 2");
            } else if (moveCount >= 33 && moveCount <= 36) {
                textField.setText("Player: Player4" + " Move: " +Integer.toString(moveCount - 32) + " Round: 2");
            } else if (moveCount >= 37 && moveCount <= 40) {
                textField.setText("Player: Player5" + " Move: " +Integer.toString(moveCount - 36) + " Round: 2");
            }
        } else if (moveCount > 40 && moveCount <= 60) {
            if (moveCount >= 41 && moveCount <= 44) {
                textField.setText("Player: Player1" + " Move: " +Integer.toString(moveCount - 40) + " Round: 3");
            } else if (moveCount >= 45 && moveCount <= 48) {
                textField.setText("Player: Player2" + " Move: " +Integer.toString(moveCount - 44) + " Round: 3");
            } else if (moveCount >= 49 && moveCount <= 52) {
                textField.setText("Player: Player3" + " Move: " +Integer.toString(moveCount - 48) + " Round: 3");
            } else if (moveCount >= 53 && moveCount <= 56) {
                textField.setText("Player: Player4" + " Move: " +Integer.toString(moveCount - 52) + " Round: 3");
            } else if (moveCount >= 57 && moveCount <= 60) {
                textField.setText("Player: Player5" + " Move: " +Integer.toString(moveCount - 56) + " Round: 3");
            }
        } else {
            textField.setText("You do not have any more move!!!");
            parameter.getControllerFrame().getlInfo2().setText("You Lost the game");
            GameOverFrame gameOver = new GameOverFrame("45 sec" );
            gameOver.instantiateFrame(false);
        }
    }

    public JPanel getpSkills() {
        return pSkills;
    }

    public void setpSkills(JPanel pSkills) {
        this.pSkills = pSkills;
    }

    public JButton getbIgloo() {
        return bIgloo;
    }

    public void setbIgloo(JButton bIgloo) {
        this.bIgloo = bIgloo;
    }

    public JButton getbCapacity() {
        return bCapacity;
    }

    public void setbCapacity(JButton bCapacity) {
        this.bCapacity = bCapacity;
    }

    public void locateFiguresOnIceberg (ArrayList<JLabel> labels) {
        int size = labels.size();
        switch (size) {
            case 1:
                labels.get(0).setBounds(37,38,35,35);
                break;
            case 2:
                labels.get(0).setBounds(12,38,35,35);
                labels.get(1).setBounds(67,38,35,35);
                break;
            case 3:
                labels.get(0).setBounds(42,12,35,35);
                labels.get(1).setBounds(15,62,35,35);
                labels.get(2).setBounds(70,62,35,35);
                break;
            case 4:
                labels.get(0).setBounds(15,15,35,35);
                labels.get(1).setBounds(70,15,35,35);
                labels.get(2).setBounds(15,70,35,35);
                labels.get(3).setBounds(70,70,35,35);
                break;
            case 5:
                labels.get(0).setBounds(2,15,35,35);
                labels.get(1).setBounds(87,15,35,35);
                labels.get(2).setBounds(45,45,35,35);
                labels.get(3).setBounds(2,75,35,35);
                labels.get(4).setBounds(87,75,35,35);
                break;
            case 6:
                labels.get(0).setBounds(7,15,35,35);
                labels.get(1).setBounds(44,15,35,35);
                labels.get(2).setBounds(81,15,35,35);
                labels.get(3).setBounds(7,75,35,35);
                labels.get(4).setBounds(44,75,35,35);
                labels.get(5).setBounds(81,75,35,35);
                break;
            case 7:
                labels.get(0).setBounds(15,10,35,35);
                labels.get(1).setBounds(70,10,35,35);
                labels.get(2).setBounds(7,10,35,35);
                labels.get(3).setBounds(44,50,35,35);
                labels.get(4).setBounds(81,50,35,35);
                labels.get(5).setBounds(15,90,35,35);
                labels.get(6).setBounds(70,90,35,35);
                break;
            case 8:
                labels.get(0).setBounds(2,15,30,30);
                labels.get(1).setBounds(32,15,30,30);
                labels.get(2).setBounds(64,15,30,30);
                labels.get(3).setBounds(95,15,30,30);
                labels.get(4).setBounds(2,70,30,30);
                labels.get(5).setBounds(32,70,30,30);
                labels.get(6).setBounds(64,70,30,30);
                labels.get(7).setBounds(96, 70, 30,30);
                break;
        }
    }
}
