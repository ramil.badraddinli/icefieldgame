package gui;

import javax.swing.*;
import java.awt.*;
import java.io.*;

public class LeaderBoardView {
    private JFrame fMain;
    private JLabel[] lResults;
    private String[] resultStrings;

    public LeaderBoardView() {
        fMain = new JFrame("LeaderBoard");
        lResults = new JLabel[10];
        resultStrings = new String[10];
    }

    public void instantiateFrame() throws IOException {
        fMain.setSize(400,550);
        fMain.getContentPane().setBackground(Color.DARK_GRAY);
        fMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        readFromFile();

        for (int i = 0; i < 10; i++) {
            lResults[i] = new JLabel(resultStrings[i]);
            lResults[i].setForeground(Color.yellow.brighter());
            lResults[i].setFont (new Font ("Serif",Font.ITALIC, 20));
            lResults[i].setBounds(20,10 + (i*50),350,30);
            fMain.add(lResults[i]);
        }

        fMain.setLayout(null);
        fMain.setLocationRelativeTo(null);
        fMain.setVisible(true);
    }

    public void readFromFile() throws IOException {
        File file = new File("src/files/leaderboard.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));
        String str;
        int i=0;
        while ((str = br.readLine()) != null) {
            String[] arrOfStr = str.split("\n", 1);
            if(i<10){
                resultStrings[i++] = arrOfStr[0];
            }
        }
    }

    public JFrame getfMain() {
        return fMain;
    }

    public JLabel[] getlResults() {
        return lResults;
    }
}
