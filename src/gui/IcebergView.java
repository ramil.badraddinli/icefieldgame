package gui;

import javax.swing.*;
import java.awt.*;

public class IcebergView {
    // Attributes
    JPanel [] icebergs;

    // Methods
    public void draw(int number_of_icebergs, JFrame frame, Parameter parameter) {
        System.out.println("#Icebergs: " + number_of_icebergs);
        switch (number_of_icebergs) {
            case 16:
                drawForEasyLevel(frame, parameter);
                break;
            case 25:
                drawForMediumLevel(frame, parameter);
                break;
            case 36:
                drawForHardLevel(frame, parameter);
                break;
        }
    }

    public void drawForEasyLevel(JFrame frame, Parameter parameter) {
        this.icebergs = new JPanel[16];
        for (int i = 0; i < icebergs.length; i++) {
            this.icebergs[i] = new JPanel();
        }

        // Locations
        this.icebergs[0].setBounds(50,30,125,125);
        this.icebergs[1].setBounds(200,30,125,125);
        this.icebergs[2].setBounds(350,30,125,125);
        this.icebergs[3].setBounds(500,30,125,125);
        this.icebergs[4].setBounds(50,200,125,125);
        this.icebergs[5].setBounds(200,200,125,125);
        this.icebergs[6].setBounds(350,200,125,125);
        this.icebergs[7].setBounds(500,200,125,125);
        this.icebergs[8].setBounds(50,350,125,125);
        this.icebergs[9].setBounds(200,350,125,125);
        this.icebergs[10].setBounds(350,350,125,125);
        this.icebergs[11].setBounds(500,350,125,125);
        this.icebergs[12].setBounds(50,500,125,125);
        this.icebergs[13].setBounds(200,500,125,125);
        this.icebergs[14].setBounds(350,500,125,125);
        this.icebergs[15].setBounds(500,500,125,125);

        for (int i = 0; i < icebergs.length; i++) {
            this.icebergs[i].setBackground(Color.WHITE);
            this.icebergs[i].setLayout(null);
            frame.add(this.icebergs[i]);
            parameter.addIcebergPanel(this.icebergs[i]);
        }
    }

    public void drawForMediumLevel(JFrame frame, Parameter parameter) {
        this.icebergs = new JPanel[25];
        for (int i = 0; i < icebergs.length; i++) {
            this.icebergs[i] = new JPanel();
        }

        // Locations of icebergs
        this.icebergs[0].setBounds(50,30,100,100);
        this.icebergs[1].setBounds(170, 30, 100, 100);
        this.icebergs[2].setBounds(290, 30,100,100);
        this.icebergs[3].setBounds(410, 30, 100, 100);
        this.icebergs[4].setBounds(530, 30, 100, 100);
        this.icebergs[5].setBounds(50, 150,100,100);
        this.icebergs[6].setBounds(170, 150,100,100);
        this.icebergs[7].setBounds(290,150,100,100);
        this.icebergs[8].setBounds(410,150,100,100);
        this.icebergs[9].setBounds(530,150,100,100);
        this.icebergs[10].setBounds(50, 270,100,100);
        this.icebergs[11].setBounds(170,270,100,100);
        this.icebergs[12].setBounds(290,270,100,100);
        this.icebergs[13].setBounds(410,270,100,100);
        this.icebergs[14].setBounds(530,270,100,100);
        this.icebergs[15].setBounds(50, 390,100,100);
        this.icebergs[16].setBounds(170,390,100,100);
        this.icebergs[17].setBounds(290,390,100,100);
        this.icebergs[18].setBounds(410,390,100,100);
        this.icebergs[19].setBounds(530,390,100,100);
        this.icebergs[20].setBounds(50, 510,100,100);
        this.icebergs[21].setBounds(170,510,100,100);
        this.icebergs[22].setBounds(290,510,100,100);
        this.icebergs[23].setBounds(410,510,100,100);
        this.icebergs[24].setBounds(530,510,100,100);

        for (int i = 0; i < icebergs.length; i++) {
            this.icebergs[i].setBackground(Color.WHITE);
            this.icebergs[i].setLayout(null);
            frame.add(this.icebergs[i]);
        }
    }

    public void drawForHardLevel(JFrame frame, Parameter parameter) {
        JPanel panel = new JPanel();
        this.icebergs = new JPanel[36];
        for (int i = 0; i < icebergs.length; i++) {
            this.icebergs[i] = new JPanel();
        }

        // Locations of icebergs
        this.icebergs[0].setBounds(30,30,80,80);
        this.icebergs[1].setBounds(134,30,80,80);
        this.icebergs[2].setBounds(238,30,80,80);
        this.icebergs[3].setBounds(342,30,80,80);
        this.icebergs[4].setBounds(446,30,80,80);
        this.icebergs[5].setBounds(550,30,80,80);
        this.icebergs[6].setBounds(30, 134, 80, 80);
        this.icebergs[7].setBounds(134, 134, 80, 80);
        this.icebergs[8].setBounds(238, 134, 80, 80);
        this.icebergs[9].setBounds(342, 134, 80, 80);
        this.icebergs[10].setBounds(446, 134, 80, 80);
        this.icebergs[11].setBounds(550, 134, 80, 80);
        this.icebergs[12].setBounds(30, 238,80,80);
        this.icebergs[13].setBounds(134, 238,80,80);
        this.icebergs[14].setBounds(238, 238,80,80);
        this.icebergs[15].setBounds(342, 238,80,80);
        this.icebergs[16].setBounds(446, 238,80,80);
        this.icebergs[17].setBounds(550, 238,80,80);
        this.icebergs[18].setBounds(30,342,80,80);
        this.icebergs[19].setBounds(134,342,80,80);
        this.icebergs[20].setBounds(238,342,80,80);
        this.icebergs[21].setBounds(342,342,80,80);
        this.icebergs[22].setBounds(446,342,80,80);
        this.icebergs[23].setBounds(550,342,80,80);
        this.icebergs[24].setBounds(30,446,80,80);
        this.icebergs[25].setBounds(134,446,80,80);
        this.icebergs[26].setBounds(238,446,80,80);
        this.icebergs[27].setBounds(342,446,80,80);
        this.icebergs[28].setBounds(446,446,80,80);
        this.icebergs[29].setBounds(550,446,80,80);
        this.icebergs[30].setBounds(30,550,80,80);
        this.icebergs[31].setBounds(134,550,80,80);
        this.icebergs[32].setBounds(238,550,80,80);
        this.icebergs[33].setBounds(342,550,80,80);
        this.icebergs[34].setBounds(446,550,80,80);
        this.icebergs[35].setBounds(550,550,80,80);

        for (int i = 0; i < icebergs.length; i++) {
            this.icebergs[i].setBackground(Color.WHITE);
            this.icebergs[i].setLayout(null);
            frame.add(this.icebergs[i]);

        }
    }

    public JPanel [] getIcebergs() {
        return this.icebergs;
    }
}
