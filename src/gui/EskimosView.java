package gui;

import elements.Iceberg;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class EskimosView {
    public void draw(Parameter parameter, JPanel [] icebergs) throws IOException {
        for (int i = 0; i < icebergs.length; i++) {
            Iceberg iceberg = parameter.getIcebergs().get(i);
            if (iceberg.getEskimos().size() > 0) {
                putEskimosOnIceberg (iceberg.getID(), icebergs, parameter);
            }
        }
    }

    public void putEskimosOnIceberg (int icebergID, JPanel [] icebergs, Parameter parameter) throws IOException {
        Iceberg iceberg = parameter.getIcebergs().get(icebergID - 1);
        JPanel icebergPanel = icebergs[icebergID - 1];
        JLabel label = new JLabel();
        BufferedImage image = ImageIO.read(new File("icons/eskimos.jpeg"));
        Image dimg = image.getScaledInstance(35, 35, Image.SCALE_SMOOTH);
        label.setBounds(37,38,35,35);
        label.setIcon(new ImageIcon(dimg));
        icebergPanel.add(label);
        iceberg.addLabel(label);
        label.setName(Integer.toString(icebergID));
        label.setToolTipText("0");
        label.addMouseListener(new FigureListener(label, parameter));
    }
}
