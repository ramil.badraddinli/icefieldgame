package gui;

import elements.Iceberg;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

public class MoveListener implements ActionListener {
    // Attributes
    Parameter parameter;
    JButton button;

    // Methods
    public MoveListener (Parameter parameter, JButton button) {
        this.parameter = parameter;
        this.button = button;
    }

    @Override
    public void actionPerformed (ActionEvent e) {
        int oldIcebergID = 0;
        int newIcebergID = 0;
        JLabel label = new JLabel();
        JButton button = (JButton)e.getSource();
        switch (button.getName()) {
            case "Left":
                 label = this.parameter.getSelectedLabel();
                 if (label != null) {
                     String id = label.getName();
                     oldIcebergID = Integer.parseInt(id);
                     if ((oldIcebergID - 1) % 4 != 0)
                         newIcebergID = oldIcebergID - 1;
                 }
                break;
            case "Right":
                label = this.parameter.getSelectedLabel();
                if (label != null) {
                    String id = label.getName();
                    oldIcebergID = Integer.parseInt(id);
                    if (oldIcebergID % 4 != 0)
                        newIcebergID = oldIcebergID + 1;
                }
                break;
            case "Down":
                label = this.parameter.getSelectedLabel();
                if (label != null) {
                    String id = label.getName();
                    oldIcebergID = Integer.parseInt(id);
                    if (oldIcebergID >=1 && oldIcebergID <= 12)
                        newIcebergID = oldIcebergID + 4;
                }
                break;
            case "Up":
                label = this.parameter.getSelectedLabel();
                if (label != null) {
                    String id = label.getName();
                    oldIcebergID = Integer.parseInt(id);
                    if (oldIcebergID >= 5 && oldIcebergID <= 16)
                        newIcebergID = oldIcebergID - 4;
                }
                break;
            case "Retrieve":
                label = this.parameter.getSelectedLabel();
                int icebergID = Integer.parseInt(label.getName());
                retrieveItemFromIceberg(parameter, icebergID - 1, label);
                break;
            case "Remove":
                label = this.parameter.getSelectedLabel();
                icebergID = Integer.parseInt(label.getName());
                removeSnowFromIceberg(parameter, icebergID - 1);
                break;
        }
        if (newIcebergID > 0 && oldIcebergID > 0) {
            moveFigureToIceberg(label, parameter, newIcebergID - 1, oldIcebergID - 1);

            Iceberg toIceberg = parameter.getIcebergs().get(oldIcebergID - 1);
            if (toIceberg.getLabels().size() > toIceberg.getCapacity()) {
                JPanel newIcebergPanel = parameter.getIcebergPanels().get(newIcebergID - 1);
                try {
                    capsizeIceberg(toIceberg.getLabels(), newIcebergPanel, parameter);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            writeInformationMessage(parameter.getMoveCount(), parameter.getControllerFrame().getlInfo(), parameter);
            parameter.increaseMoveCount();
        }
    }

    public void writeInformationMessage(int moveCount, JLabel textField, Parameter parameter) {
        if (moveCount <= 20) {
            if (moveCount >= 1 && moveCount <= 4) {
                textField.setText("Player: Player1" + " Move: " +Integer.toString(moveCount) + " Round: 1");
            } else if (moveCount >= 5 && moveCount <= 8) {
                textField.setText("Player: Player2" + " Move: " +Integer.toString(moveCount - 4) + " Round: 1");
            } else if (moveCount >= 9 && moveCount <= 12) {
                textField.setText("Player: Player3" + " Move: " +Integer.toString(moveCount - 8) + " Round: 1");
            } else if (moveCount >= 13 && moveCount <= 16) {
                textField.setText("Player: Player4" + " Move: " +Integer.toString(moveCount - 12) + " Round: 1");
            } else if (moveCount >= 17 && moveCount <= 20) {
                textField.setText("Player: Player5" + " Move: " +Integer.toString(moveCount - 16) + " Round: 1");
            }
        } else if (moveCount > 20 && moveCount <= 40) {
            if (moveCount >= 21 && moveCount <= 24) {
                textField.setText("Player: Player1" + " Move: " +Integer.toString(moveCount - 20) + " Round: 2");
            } else if (moveCount >= 25 && moveCount <= 28) {
                textField.setText("Player: Player2" + " Move: " +Integer.toString(moveCount - 24) + " Round: 2");
            } else if (moveCount >= 29 && moveCount <= 32) {
                textField.setText("Player: Player3" + " Move: " +Integer.toString(moveCount - 28) + " Round: 2");
            } else if (moveCount >= 33 && moveCount <= 36) {
                textField.setText("Player: Player4" + " Move: " +Integer.toString(moveCount - 32) + " Round: 2");
            } else if (moveCount >= 37 && moveCount <= 40) {
                textField.setText("Player: Player5" + " Move: " +Integer.toString(moveCount - 36) + " Round: 2");
            }
        } else if (moveCount > 40 && moveCount <= 60) {
            if (moveCount >= 41 && moveCount <= 44) {
                textField.setText("Player: Player1" + " Move: " +Integer.toString(moveCount - 40) + " Round: 3");
            } else if (moveCount >= 45 && moveCount <= 48) {
                textField.setText("Player: Player2" + " Move: " +Integer.toString(moveCount - 44) + " Round: 3");
            } else if (moveCount >= 49 && moveCount <= 52) {
                textField.setText("Player: Player3" + " Move: " +Integer.toString(moveCount - 48) + " Round: 3");
            } else if (moveCount >= 53 && moveCount <= 56) {
                textField.setText("Player: Player4" + " Move: " +Integer.toString(moveCount - 52) + " Round: 3");
            } else if (moveCount >= 57 && moveCount <= 60) {
                textField.setText("Player: Player5" + " Move: " +Integer.toString(moveCount - 56) + " Round: 3");
            }
        } else {
            textField.setText("You do not have any more move!!!");
            parameter.getControllerFrame().getlInfo2().setText("You Lost the game");
        }
     }

    public void moveFigureToIceberg(JLabel figure, Parameter parameter, int newIcebergID, int oldIcebergID) {
        Iceberg fromIceberg = parameter.getIcebergs().get(oldIcebergID);
        Iceberg toIceberg = parameter.getIcebergs().get(newIcebergID);
        JPanel oldIcebergPanel = parameter.getIcebergPanels().get(oldIcebergID);
        JPanel newIcebergPanel = parameter.getIcebergPanels().get(newIcebergID);

        if ((fromIceberg.getID() == 6 && toIceberg.getID() == 10) || (fromIceberg.getID() == 10 && toIceberg.getID() == 6)) {
            refreshOldIcebergPanel(oldIcebergPanel, fromIceberg, figure);
            parameter.setSelectedPanelId(fromIceberg.getID() - 1);
            figure.setBounds(262,326,24,24);
            parameter.getFrame().add(figure);
            parameter.getFrame().revalidate();
            parameter.getFrame().repaint();
        } else {
            refreshOldIcebergPanel(oldIcebergPanel, fromIceberg, figure);
            refreshNewIcebergPanel(newIcebergPanel, toIceberg, figure);
            figure.setName(Integer.toString(newIcebergID + 1));
        }

    }

    public void refreshOldIcebergPanel (JPanel oldIcebergPanel, Iceberg fromIceberg, JLabel figure) {
        oldIcebergPanel.remove(figure);
        int index = Integer.parseInt(figure.getToolTipText());
        fromIceberg.getLabels().remove(index);
        for (int i = 0; i < fromIceberg.getLabels().size(); i++) {
            fromIceberg.getLabels().get(i).setToolTipText(Integer.toString(i));
        }
        if (fromIceberg.getLabels().size() > 0)
            locateFiguresOnIceberg(fromIceberg.getLabels());
        oldIcebergPanel.revalidate();
        oldIcebergPanel.repaint();
    }

    public void refreshNewIcebergPanel (JPanel newIcebergPanel, Iceberg toIceberg, JLabel figure) {
        toIceberg.getLabels().add(figure);
        figure.setToolTipText(Integer.toString(toIceberg.getLabels().size()-1));
        locateFiguresOnIceberg(toIceberg.getLabels());
        newIcebergPanel.add(figure);
        newIcebergPanel.revalidate();
        newIcebergPanel.repaint();
    }

    public void capsizeIceberg (ArrayList<JLabel> figures, JPanel newIcebergPanel,Parameter parameter) throws IOException {
        for (int i = 0; i < figures.size(); i++) {
            newIcebergPanel.remove(figures.get(i));
            newIcebergPanel.setVisible(false);
            newIcebergPanel.revalidate();
            newIcebergPanel.repaint();
        }
        tumbleFiguresToSea(newIcebergPanel, figures);
        for (int i = 0; i < figures.size(); i++) {
            parameter.getFrame().add(figures.get(i));
        }
        parameter.getFrame().revalidate();
        parameter.getFrame().repaint();
        parameter.getControllerFrame().getlInfo2().setText("The iceberg capsized, YOU LOST");
        GameOverFrame gameOver = new GameOverFrame("45 sec" );
        gameOver.instantiateFrame(false);
    }

    public void tumbleFiguresToSea (JPanel newIcebergPanel, ArrayList<JLabel> figures) {
        int size = figures.size();
        switch (size) {
            case 1:
                figures.get(0).setBounds(newIcebergPanel.getX(), newIcebergPanel.getHeight(), 40,40);
                break;
            case 2:
                figures.get(0).setBounds(newIcebergPanel.getX(), newIcebergPanel.getY(), 40,40);
                figures.get(1).setBounds(newIcebergPanel.getX() + 45, newIcebergPanel.getY(), 40,40);
                break;
            case 3:
                figures.get(0).setBounds(newIcebergPanel.getX(), newIcebergPanel.getY(), 40,40);
                figures.get(1).setBounds(newIcebergPanel.getX() + 45, newIcebergPanel.getY(), 40,40);
                figures.get(2).setBounds(newIcebergPanel.getX(), newIcebergPanel.getY() + 45, 40,40);
                break;
            case 4:
                figures.get(0).setBounds(newIcebergPanel.getX(), newIcebergPanel.getY(), 40,40);
                figures.get(1).setBounds(newIcebergPanel.getX() + 45, newIcebergPanel.getY(), 40,40);
                figures.get(2).setBounds(newIcebergPanel.getX(), newIcebergPanel.getY() + 45, 40,40);
                figures.get(3).setBounds(newIcebergPanel.getX() + 45, newIcebergPanel.getY() + 45, 40,40);
                break;
        }
    }

    public void removeSnowFromIceberg (Parameter parameter, int icebergID) {
        Iceberg iceberg = parameter.getIcebergs().get(icebergID);
        if (iceberg.getAmountOfSnow() > 0) {
            iceberg.removeSnow(1);
            parameter.getControllerFrame().getlInfo2().setText("Amount of snow: " + iceberg.getAmountOfSnow());
        } else if (iceberg.getAmountOfSnow() == 0) {
            parameter.getControllerFrame().getlInfo2().setText("There is no snow any more, retrieve item");
        }
    }

    public void retrieveItemFromIceberg (Parameter parameter, int icebergID, JLabel figure) {
        Iceberg iceberg = parameter.getIcebergs().get(icebergID);
        if (iceberg.getAmountOfSnow() == 0) {
            if (iceberg.getItems().size() > 0) {
                if (iceberg.getFoods().size() > 0) {
                    parameter.getControllerFrame().getlInfo2().setText("The food is retrieved");
                } else if (iceberg.getDivingSuits().size() > 0) {
                    parameter.getControllerFrame().getlInfo2().setText("The diving suit is retrieved");
                } else if (iceberg.getRopes().size() > 0) {
                    parameter.getControllerFrame().getlInfo2().setText("The rope is retrieved");
                } else if (iceberg.getShovels().size() > 0) {
                    parameter.getControllerFrame().getlInfo2().setText("The shovel is retrived");
                } else if (iceberg.getFlareGuns().size() > 0) {
                    parameter.getControllerFrame().getlInfo2().setText("The part of flaregun is retrieved");
                    parameter.increaseCompletedParts();
                    if (parameter.getCompletedParts() == 3) {
                        winTheGame(parameter);
                    }
                }
            } else {
                parameter.getControllerFrame().getlInfo2().setText("There is no item on this iceberg");
            }
        } else {
            parameter.getControllerFrame().getlInfo2().setText("There is a snow on iceberg, the item cannot be retrieved");
        }
    }

    public void winTheGame(Parameter parameter) {
        Iceberg stableIceberg = new Iceberg();
        for (int i = 0; i < parameter.getIcebergs().size(); i++) {
            if (parameter.getIcebergs().get(i).isStable()) {
                stableIceberg = parameter.getIcebergs().get(i);
                break;
            }
        }

        for (int i = 0; i < parameter.getIcebergs().size(); i++) {
            if (i != stableIceberg.getID() - 1) {
                for (int j = 0; j < parameter.getIcebergs().get(i).getLabels().size(); j++) {
                    JLabel figure = parameter.getIcebergs().get(i).getLabels().get(j);
                    moveFigureToIceberg(figure, parameter, stableIceberg.getID() - 1, i);
                }
                parameter.getControllerFrame().getlInfo2().setText("FlareGun is fired. Congratulations, YOU WON!!");
            }
        }

        GameOverFrame gameOver = new GameOverFrame("45 sec" );
        gameOver.instantiateFrame(true);
    }

    public void locateFiguresOnIceberg (ArrayList<JLabel> labels) {
        int size = labels.size();
        switch (size) {
            case 1:
                labels.get(0).setBounds(37,38,35,35);
                break;
            case 2:
                labels.get(0).setBounds(12,38,35,35);
                labels.get(1).setBounds(67,38,35,35);
                break;
            case 3:
                labels.get(0).setBounds(42,12,35,35);
                labels.get(1).setBounds(15,62,35,35);
                labels.get(2).setBounds(70,62,35,35);
                break;
            case 4:
                labels.get(0).setBounds(15,15,35,35);
                labels.get(1).setBounds(70,15,35,35);
                labels.get(2).setBounds(15,70,35,35);
                labels.get(3).setBounds(70,70,35,35);
                break;
            case 5:
                labels.get(0).setBounds(2,15,35,35);
                labels.get(1).setBounds(87,15,35,35);
                labels.get(2).setBounds(45,45,35,35);
                labels.get(3).setBounds(2,75,35,35);
                labels.get(4).setBounds(87,75,35,35);
                break;
            case 6:
                labels.get(0).setBounds(7,15,35,35);
                labels.get(1).setBounds(44,15,35,35);
                labels.get(2).setBounds(81,15,35,35);
                labels.get(3).setBounds(7,75,35,35);
                labels.get(4).setBounds(44,75,35,35);
                labels.get(5).setBounds(81,75,35,35);
                break;
            case 7:
                labels.get(0).setBounds(15,10,35,35);
                labels.get(1).setBounds(70,10,35,35);
                labels.get(2).setBounds(7,10,35,35);
                labels.get(3).setBounds(44,50,35,35);
                labels.get(4).setBounds(81,50,35,35);
                labels.get(5).setBounds(15,90,35,35);
                labels.get(6).setBounds(70,90,35,35);
                break;
            case 8:
                labels.get(0).setBounds(2,15,30,30);
                labels.get(1).setBounds(32,15,30,30);
                labels.get(2).setBounds(64,15,30,30);
                labels.get(3).setBounds(95,15,30,30);
                labels.get(4).setBounds(2,70,30,30);
                labels.get(5).setBounds(32,70,30,30);
                labels.get(6).setBounds(64,70,30,30);
                labels.get(7).setBounds(96, 70, 30,30);
                break;
        }
    }
}
