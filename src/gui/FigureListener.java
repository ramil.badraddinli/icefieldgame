package gui;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FigureListener implements MouseListener {
    // Attributes
    Parameter parameter;
    JLabel label;

    // Methods
    public FigureListener (JLabel label, Parameter parameter) {
        this.label = label;
        this.parameter = parameter;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.parameter.setSelectedLabel(this.label);
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
