package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameOverFrame {
    private JFrame fMain;
    private JLabel lWin;
    private JLabel lLose;
    private JButton bNewGame;
    private JLabel lResult;

    public GameOverFrame(String result) {
        fMain = new JFrame("game.Game Over");
        lWin = new JLabel("You Won!!");
        lLose = new JLabel("You Lost!!");
        lResult = new JLabel(result);
        bNewGame = new JButton("New game.Game");
    }

    public void instantiateFrame(boolean isWon) {
        fMain.setSize(450, 250);
        fMain.getContentPane().setBackground(Color.DARK_GRAY);
        fMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        lWin.setForeground(Color.yellow.brighter());
        lWin.setFont(new Font("Serif", Font.ITALIC, 25));
        lWin.setBounds(20, 10, 400, 40);

        lLose.setForeground(Color.yellow.brighter());
        lLose.setFont(new Font("Serif", Font.ITALIC, 25));
        lLose.setBounds(20, 10, 400, 40);

        lResult.setForeground(Color.yellow.brighter());
        lResult.setFont(new Font("Serif", Font.ITALIC, 25));
        lResult.setBounds(20, 70, 400, 40);

        bNewGame.setBounds(20, 130, 400, 50);
        bNewGame.setBackground(Color.pink);
        bNewGame.setForeground(Color.darkGray);
        bNewGame.setFont(new Font("Times New Roman", Font.ITALIC, 20));
        bNewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                WelcomeView welcomeView = new WelcomeView();
                welcomeView.instantiateFrame();
            }
        });

        fMain.add(lResult);
        fMain.add(bNewGame);
        if (isWon) fMain.add(lWin);
        else fMain.add(lLose);

        fMain.setLayout(null);
        fMain.setLocationRelativeTo(null);
        fMain.setVisible(true);
    }

    public JFrame getfMain() {
        return fMain;
    }

    public JLabel getlWin() {
        return lWin;
    }

    public JLabel getlLose() {
        return lLose;
    }

    public JButton getbNewGame() {
        return bNewGame;
    }

    public JLabel getlResult() {
        return lResult;
    }
}
