package gui;

import elements.Iceberg;
import figures.Eskimos;
import figures.Explorer;
import items.DivingSuit;
import items.Food;
import items.Rope;
import items.Shovel;

import javax.swing.*;
import java.util.ArrayList;

public class Parameter {
    // Attributes
    private int number_of_icebergs;
    private int number_of_eskimos;
    private int number_of_explorers;
    private int number_of_food;
    private int number_of_divingsuits;
    private int number_of_ropes;
    private int number_of_shovels;
    private int completedParts;
    private int number_of_holes;
    private int moveCount;
    private ControllerFrame controllerFrame;
    private JFrame frame;
    private JLabel selectedLabel;
    private int selectedPanelId;
    private JLabel fallenFigure;
    private String difficultyLevel;
    private ArrayList<Iceberg> icebergs;
    private ArrayList<Eskimos> eskimos;
    private ArrayList<Explorer> explorers;
    private ArrayList<Food> foods;
    private ArrayList<DivingSuit> divingSuits;
    private ArrayList<Rope> ropes;
    private ArrayList<Shovel> shovels;
    private ArrayList<JPanel> icebergPanels;

    // Constructor
    public Parameter() {
        this.icebergs = new ArrayList<Iceberg>();
        this.eskimos = new ArrayList<Eskimos>();
        this.explorers = new ArrayList<Explorer>();
        this.foods = new ArrayList<Food>();
        this.divingSuits = new ArrayList<DivingSuit>();
        this.ropes = new ArrayList<Rope>();
        this.shovels = new ArrayList<Shovel>();
        this.icebergPanels = new ArrayList<JPanel>();
        this.moveCount = 1;
        this.completedParts = 0;
    }

    // Methods
    public void setNumber_of_icebergs(int number_of_icebergs) {
        this.number_of_icebergs = number_of_icebergs;
    }

    public Iceberg getIcebergByID(int ID){
        for (Iceberg iceberg: icebergs) {
            if(iceberg.getID()==ID) return iceberg;
        }
        return null;
    }
    public int getNumber_of_icebergs() {
        return this.number_of_icebergs;
    }

    public void setNumber_of_eskimos(int number_of_eskimos) {
        this.number_of_eskimos = number_of_eskimos;
    }

    public int getNumber_of_eskimos() {
        return this.number_of_eskimos;
    }

    public void setNumber_of_explorers(int number_of_explorers) {
        this.number_of_explorers = number_of_explorers;
    }

    public int getNumber_of_explorers() {
        return this.number_of_explorers;
    }

    public void setNumber_of_food(int number_of_food) {
        this.number_of_food = number_of_food;
    }

    public int getNumber_of_food() {
        return this.number_of_food;
    }

    public void setNumber_of_divingsuits(int number_of_divingsuits) {
        this.number_of_divingsuits = number_of_divingsuits;
    }

    public int getNumber_of_divingsuits() {
        return this.number_of_divingsuits;
    }

    public void setNumber_of_ropes(int number_of_ropes) {
        this.number_of_ropes = number_of_ropes;
    }

    public int getNumber_of_ropes() {
        return this.number_of_ropes;
    }

    public void setNumber_of_shovels(int number_of_shovels) {
        this.number_of_shovels = number_of_shovels;
    }

    public int getNumber_of_shovels() {
        return this.number_of_shovels;
    }

    public void setDifficultyLevel(String level) {
        this.difficultyLevel = level;
    }

    public String getDifficultyLevel() {
        return this.difficultyLevel;
    }

    public void addIcebergs(Iceberg iceberg) {
        this.icebergs.add(iceberg);
    }

    public ArrayList<Iceberg> getIcebergs() {
        return this.icebergs;
    }

    public void addEskimo (Eskimos eskimos) {
        this.eskimos.add(eskimos);
    }

    public ArrayList<Eskimos> getEskimos() {
        return this.eskimos;
    }

    public void addExplorer (Explorer explorer) {
        this.explorers.add(explorer);
    }

    public ArrayList<Explorer> getExplorers() {
        return this.explorers;
    }

    public void addFood (Food food) {
        this.foods.add(food);
    }

    public ArrayList<Food> getFoods() {
        return this.foods;
    }

    public void addDivingSuit (DivingSuit divingSuit) {
        divingSuits.add(divingSuit);
    }

    public ArrayList<DivingSuit> getDivingSuits() {
        return this.divingSuits;
    }

    public void addRope (Rope rope) {
        this.ropes.add(rope);
    }

    public ArrayList<Rope> getRopes() {
        return this.ropes;
    }

    public void addShovel (Shovel shovel) {
        this.shovels.add(shovel);
    }

    public ArrayList<Shovel> getShovels() {
        return this.shovels;
    }

    public void addIcebergPanel (JPanel iceberg) {
        this.icebergPanels.add(iceberg);
    }

    public ArrayList<JPanel> getIcebergPanels() {
        return this.icebergPanels;
    }

    public void setSelectedLabel(JLabel label) {
        this.selectedLabel = label;
    }

    public JLabel getSelectedLabel() {
        return this.selectedLabel;
    }

    public void setFrame (JFrame frame) {
        this.frame = frame;
    }

    public JFrame getFrame() {
        return this.frame;
    }

    public void increaseMoveCount() {
        this.moveCount += 1;
    }

    public int getMoveCount() {
        return this.moveCount;
    }

    public void setControllerFrame(ControllerFrame controllerFrame) {
        this.controllerFrame = controllerFrame;
    }

    public ControllerFrame getControllerFrame() {
        return this.controllerFrame;
    }

    public void setNumber_of_holes (int number_of_holes) {
        this.number_of_holes = number_of_holes;
    }

    public int getNumber_of_holes() {
        return this.number_of_holes;
    }

    public void increaseCompletedParts() {
        this.completedParts += 1;
    }

    public int getCompletedParts() {
        return this.completedParts;
    }

    public void setFallenFigure (JLabel figure) {
        this.fallenFigure = figure;
    }

    public JLabel getFallenFigure () {
        return this.fallenFigure;
    }

    public void setSelectedPanelId (int panelId) {
        this.selectedPanelId = panelId;
    }

    public int getSelectedPanelId() {
        return this.selectedPanelId;
    }
}
