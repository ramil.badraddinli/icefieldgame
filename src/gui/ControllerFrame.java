package gui;

import game.Game;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class ControllerFrame {
    private Game game;
    private Parameter parameter;
    private JFrame mainFrame;
    private SkillView skillView;
    private ItemView itemView;
    private MoveView moveView;
    private JLabel lInfo;
    private JLabel lInfo2;
    private JLabel lError;

    public ControllerFrame(Parameter parameter, Game game) {
        this.parameter = parameter;
        this.game = game;
        mainFrame = new JFrame();
        skillView = new SkillView(parameter, this);
        itemView = new ItemView(parameter, this);
        moveView = new MoveView(parameter);
        lInfo = new JLabel("Information Message");
        lInfo2 = new JLabel("Second Information");
        lError = new JLabel("Error Message");
    }

    public Game getGame() {
        return game;
    }

    public Parameter getParameter() {
        return parameter;
    }

    public JLabel getlInfo2() {
        return lInfo2;
    }

    public void instantianateFrame() throws IOException {
        mainFrame.setTitle("Controller");
        mainFrame.getContentPane().setBackground(Color.BLUE);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        skillView.locateElements();
        itemView.locateElements();
        moveView.locateElements();

        moveView.addListeners();

        lInfo.setForeground(Color.yellow.brighter());
        lInfo.setFont (new Font ("Serif",Font.ITALIC, 20));
        lInfo.setBounds(20,200,550,50);

        lInfo2.setForeground(Color.yellow.brighter());
        lInfo2.setFont (new Font ("Serif",Font.ITALIC, 20));
        lInfo2.setBounds(20,250,550,50);

        lError.setForeground(Color.RED.brighter());
        lError.setFont (new Font ("Serif",Font.ITALIC, 20));
        lError.setBounds(20,300,550,50);

        mainFrame.add(lError);
        mainFrame.add(lInfo);
        mainFrame.add(lInfo2);
        mainFrame.add(skillView.getpSkills());
        mainFrame.add(itemView.getpItems());
        mainFrame.add(moveView.getpMoves());

        mainFrame.getContentPane().setBackground(Color.DARK_GRAY);
        mainFrame.setSize(700, 400);
        mainFrame.setLayout(null);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    public void setMainFrame(JFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    public SkillView getSkillView() {
        return skillView;
    }

    public void setSkillView(SkillView skillView) {
        this.skillView = skillView;
    }

    public ItemView getItemView() {
        return itemView;
    }

    public void setItemView(ItemView itemView) {
        this.itemView = itemView;
    }

    public MoveView getMoveView() {
        return moveView;
    }

    public void setMoveView(MoveView moveView) {
        this.moveView = moveView;
    }

    public JLabel getlInfo() {
        return lInfo;
    }

    public JLabel getlError() {
        return lError;
    }
}
