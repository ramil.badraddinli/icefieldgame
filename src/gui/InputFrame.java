package gui;

import game.Game;
import game.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class InputFrame {
    private JFrame fMain;
    private JLabel lTeamName;
    private JTextField tfTeamName;
    private JLabel[] lPlayerNames;
    private JTextField[] tfPlayerNames;
    private JButton bBegin;
    private Game game;

    public InputFrame(String level) {
        fMain = new JFrame("Input Frame");
        lPlayerNames = new JLabel[5];
        tfPlayerNames = new JTextField[5];
        lTeamName = new JLabel("Team Name: ");
        tfTeamName = new JTextField();
        bBegin = new JButton("Begin game.Game");
        game = new Game(level);
    }

    public void instantiateFrame(){
        fMain.setSize(500,500);
        fMain.getContentPane().setBackground(Color.DARK_GRAY);
        fMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        lTeamName.setForeground(Color.yellow.brighter());
        lTeamName.setFont (new Font ("Serif",Font.ITALIC, 20));
        lTeamName.setBounds(20,10,200,40);

        tfTeamName.setBounds(250, 10 , 220, 40);

        for (int i = 0; i < 5; i++) {
            lPlayerNames[i] = new JLabel("game.Player #" + (i+1) + " name: ");
            lPlayerNames[i].setForeground(Color.yellow.brighter());
            lPlayerNames[i].setFont (new Font ("Serif",Font.ITALIC, 20));
            lPlayerNames[i].setBounds(20,70 + (i*60),200,40);
            fMain.add(lPlayerNames[i]);
        }

        for (int i = 0; i < 5; i++) {
            tfPlayerNames[i] = new JTextField();
            tfPlayerNames[i].setBounds(250,70 + (i*60),220,40);
            fMain.add(tfPlayerNames[i]);
        }

        bBegin.setBounds(20,380,450,50);
        bBegin.setBackground(Color.pink);
        bBegin.setForeground(Color.darkGray);
        bBegin.setFont (new Font ("Times New Roman",Font.ITALIC, 18));
        bBegin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    game.setTeamName(tfTeamName.getText());
                    for (JTextField username: tfPlayerNames) {
                        game.addPlayer(new Player(username.getText()));
                    }
                    System.out.println(game.getParameter().getNumber_of_icebergs());
                    fMain.setVisible(false);
                    fMain.dispose();
                    game.startGame();
                    setGame(game);
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        });

        fMain.add(lTeamName);
        fMain.add(tfTeamName);
        fMain.add(bBegin);

        fMain.setLayout(null);
        fMain.setLocationRelativeTo(null);
        fMain.setVisible(true);
    }

    public JFrame getfMain() {
        return fMain;
    }

    public JLabel getlTeamName() {
        return lTeamName;
    }

    public JTextField getTfTeamName() {
        return tfTeamName;
    }

    public JLabel[] getlPlayerNames() {
        return lPlayerNames;
    }

    public JTextField[] getTfPlayerNames() {
        return tfPlayerNames;
    }

    public JButton getbBegin() {
        return bBegin;
    }

    public void setGame (Game game) {
        this.game = game;
    }

    public Game getGame() {
        return this.game;
    }
}
