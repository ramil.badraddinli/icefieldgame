package gui;

import elements.Iceberg;
import figures.Figure;
import game.Game;
import items.DivingSuit;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ItemView {
    Game game;
    Parameter parameter;
    private JPanel pItems;
    private JButton bFood;
    private JButton bDivingSuit;
    private JButton bRope;
    private JButton bShovel;
    private JButton bFlareGun;
    private String message;
    private ControllerFrame controllerFrame;

    public ItemView(Parameter parameter, ControllerFrame controllerFrame) {
        pItems = new JPanel();
        bFood = new JButton();
        bDivingSuit = new JButton();
        bFlareGun = new JButton();
        bRope = new JButton();
        bShovel = new JButton();
        this.parameter = parameter;
        this.controllerFrame = controllerFrame;
    }

    public void locateElements() throws IOException {
        pItems.setBounds(190, 0, 240, 200);
        pItems.setBackground(null);
        pItems.setLayout(null);

//        bRope.setText("Rope");
//        bDivingSuit.setText("Diving Suit");
//        bFlareGun.setText("Flare Gun");
//        bFood.setText("Food");
//        bShovel.setText("Shovel");

        bRope.setForeground(Color.LIGHT_GRAY);
        bDivingSuit.setForeground(Color.LIGHT_GRAY);
        bShovel.setForeground(Color.LIGHT_GRAY);
        bFood.setForeground(Color.LIGHT_GRAY);
        bFlareGun.setForeground(Color.LIGHT_GRAY);

        bFood.setBackground(Color.BLACK);
        bRope.setBackground(Color.BLACK);
        bFlareGun.setBackground(Color.BLACK);
        bDivingSuit.setBackground(Color.BLACK);
        bShovel.setBackground(Color.BLACK);

        bFood.setBounds(10, 10, 60, 60);
        bShovel.setBounds(80, 10, 60, 60);
        bDivingSuit.setBounds(10, 100, 60, 60);
        bRope.setBounds(80, 100, 60, 60);
        bFlareGun.setBounds(160, 60, 60, 60);

        addListeners();
        addIcons();

        pItems.add(bDivingSuit);
        pItems.add(bFlareGun);
        pItems.add(bRope);
        pItems.add(bFood);
        pItems.add(bShovel);
    }

    public void addListeners(){
        bFood.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               int icebergID = Integer.valueOf(parameter.getSelectedLabel().getName()) - 1;
               int figureIndex = Integer.valueOf(parameter.getSelectedLabel().getToolTipText());
                Figure figure = parameter.getIcebergByID(icebergID).getFigures().
                       get(figureIndex);
                figure.increaseBodyHeat(1);
               int bodyHeat =  parameter.getIcebergByID(icebergID).getFigures().
                       get(figureIndex).getBodyHeat();
//               if(figure.hasItem("Food") || true){
                   figure.increaseBodyHeat(1);
                   controllerFrame.getlInfo2().setText("Selected Figure's body heat: " + bodyHeat);
                   //figure.removeItem("Food");
//               }
//               else{
//                   controllerFrame.getlInfo2().setText("Selected Figure does not have Food");
//               }

            }
        });

        bShovel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int icebergID = Integer.valueOf(parameter.getSelectedLabel().getName());
                parameter.getIcebergByID(icebergID).removeSnow(2);
                int figureIndex = Integer.valueOf(parameter.getSelectedLabel().getToolTipText());

                Figure figure = parameter.getIcebergByID(icebergID).getFigures().
                        get(figureIndex);
//                if(figure.hasItem("Rope") ){
                    parameter.getIcebergByID(icebergID).removeSnow(1);
                    controllerFrame.getlInfo().setText("Snow removed!!");
                    controllerFrame.getlInfo2().setText("Iceberg has " +
                            parameter.getIcebergByID(icebergID).getAmountOfSnow()+ " unit of snow");
                    //figure.removeItem("Rope");
//                }
//                else{
//                    controllerFrame.getlInfo2().setText("Selected Figure does not have Shovel!!");
//                }
            }
        });

        bDivingSuit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JLabel label = parameter.getSelectedLabel();
                Iceberg iceberg = parameter.getIcebergs().get(parameter.getSelectedPanelId());
                Figure figure1 = iceberg.getFigures().get(0);
                JPanel newIcebergPanel = parameter.getIcebergPanels().get(parameter.getSelectedPanelId());
                if (figure1.getItems().size() > 0) {
                    if (figure1.getItems().get(0) instanceof DivingSuit) {
                        parameter.getFrame().remove(label);
                        parameter.getFrame().revalidate();
                        parameter.getFrame().repaint();
                        iceberg.getLabels().add(label);
                        label.setToolTipText(Integer.toString(iceberg.getLabels().size() - 1));
                        locateFiguresOnIceberg(iceberg.getLabels());
                        newIcebergPanel.add(label);
                        newIcebergPanel.revalidate();
                        newIcebergPanel.repaint();
                    } else {
                        parameter.getControllerFrame().getlInfo2().setText("The figure does not have diving suit");
                    }
                } else {
                        parameter.getControllerFrame().getlInfo2().setText("The figure does not have diving suit");
                }
            }
        });

        bRope.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Iceberg iceberg1 = parameter.getIcebergs().get(5);
                Iceberg iceberg2 = parameter.getIcebergs().get(9);
                JPanel newIcebergPanel = parameter.getIcebergPanels().get(5);
                if (iceberg1.getRopes().size() > 0) {
                    JLabel figure = parameter.getSelectedLabel();
                    parameter.getFrame().remove(figure);
                    parameter.getFrame().revalidate();
                    parameter.getFrame().repaint();
                    iceberg1.getLabels().add(figure);
                    figure.setToolTipText(Integer.toString(iceberg1.getLabels().size()-1));
                    locateFiguresOnIceberg(iceberg1.getLabels());
                    newIcebergPanel.add(figure);
                    newIcebergPanel.revalidate();
                    newIcebergPanel.repaint();
                    figure.setName("6");
                } else if (iceberg2.getRopes().size() > 0) {
                    JLabel figure = parameter.getSelectedLabel();
                    parameter.getFrame().remove(figure);
                    parameter.getFrame().revalidate();
                    parameter.getFrame().repaint();
                    iceberg2.getLabels().add(figure);
                    figure.setToolTipText(Integer.toString(iceberg2.getLabels().size()-1));
                    locateFiguresOnIceberg(iceberg2.getLabels());
                    newIcebergPanel.add(figure);
                    newIcebergPanel.revalidate();
                    newIcebergPanel.repaint();
                    figure.setName("10");
                } else {
                    parameter.getControllerFrame().getlInfo2().setText("There is no rope either, YOU LOST THE GAME!!");
                    GameOverFrame gameOver = new GameOverFrame("45 sec" );
                    gameOver.instantiateFrame(false);
                }
            }
        });
    }

    public void locateFiguresOnIceberg (ArrayList<JLabel> labels) {
        int size = labels.size();
        switch (size) {
            case 1:
                labels.get(0).setBounds(37,38,35,35);
                break;
            case 2:
                labels.get(0).setBounds(12,38,35,35);
                labels.get(1).setBounds(67,38,35,35);
                break;
            case 3:
                labels.get(0).setBounds(42,12,35,35);
                labels.get(1).setBounds(15,62,35,35);
                labels.get(2).setBounds(70,62,35,35);
                break;
            case 4:
                labels.get(0).setBounds(15,15,35,35);
                labels.get(1).setBounds(70,15,35,35);
                labels.get(2).setBounds(15,70,35,35);
                labels.get(3).setBounds(70,70,35,35);
                break;
            case 5:
                labels.get(0).setBounds(2,15,35,35);
                labels.get(1).setBounds(87,15,35,35);
                labels.get(2).setBounds(45,45,35,35);
                labels.get(3).setBounds(2,75,35,35);
                labels.get(4).setBounds(87,75,35,35);
                break;
            case 6:
                labels.get(0).setBounds(7,15,35,35);
                labels.get(1).setBounds(44,15,35,35);
                labels.get(2).setBounds(81,15,35,35);
                labels.get(3).setBounds(7,75,35,35);
                labels.get(4).setBounds(44,75,35,35);
                labels.get(5).setBounds(81,75,35,35);
                break;
            case 7:
                labels.get(0).setBounds(15,10,35,35);
                labels.get(1).setBounds(70,10,35,35);
                labels.get(2).setBounds(7,10,35,35);
                labels.get(3).setBounds(44,50,35,35);
                labels.get(4).setBounds(81,50,35,35);
                labels.get(5).setBounds(15,90,35,35);
                labels.get(6).setBounds(70,90,35,35);
                break;
            case 8:
                labels.get(0).setBounds(2,15,30,30);
                labels.get(1).setBounds(32,15,30,30);
                labels.get(2).setBounds(64,15,30,30);
                labels.get(3).setBounds(95,15,30,30);
                labels.get(4).setBounds(2,70,30,30);
                labels.get(5).setBounds(32,70,30,30);
                labels.get(6).setBounds(64,70,30,30);
                labels.get(7).setBounds(96, 70, 30,30);
                break;
        }
    }

    public void addIcons() throws IOException {
        BufferedImage bufferedImage = ImageIO.read(new File("icons/food.jpeg"));
        Image image = bufferedImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
        bFood.setIcon(new ImageIcon(image));

        bufferedImage = ImageIO.read(new File("icons/rope.jpeg"));
        image = bufferedImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
        bRope.setIcon(new ImageIcon(image));

        bufferedImage = ImageIO.read(new File("icons/divingsuit.jpeg"));
        image = bufferedImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
        bDivingSuit.setIcon(new ImageIcon(image));

        bufferedImage = ImageIO.read(new File("icons/shovel.jpeg"));
        image = bufferedImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
        bShovel.setIcon(new ImageIcon(image));

        bufferedImage = ImageIO.read(new File("icons/flaregun.jpeg"));
        image = bufferedImage.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
        bFlareGun.setIcon(new ImageIcon(image));

    }
    public JPanel getpItems() {
        return pItems;
    }

    public void setpItems(JPanel pItems) {
        this.pItems = pItems;
    }

    public JButton getbFood() {
        return bFood;
    }

    public void setbFood(JButton bFood) {
        this.bFood = bFood;
    }

    public JButton getbDivingSuit() {
        return bDivingSuit;
    }

    public void setbDivingSuit(JButton bDivingSuit) {
        this.bDivingSuit = bDivingSuit;
    }

    public JButton getbRope() {
        return bRope;
    }

    public void setbRope(JButton bRope) {
        this.bRope = bRope;
    }

    public JButton getbShovel() {
        return bShovel;
    }

    public void setbShovel(JButton bShovel) {
        this.bShovel = bShovel;
    }

    public JButton getbFlareGun() {
        return bFlareGun;
    }

    public void setbFlareGun(JButton bFlareGun) {
        this.bFlareGun = bFlareGun;
    }
}
