package game;

import elements.Hole;
import elements.Iceberg;
import figures.Eskimos;
import figures.Explorer;
import gui.ControllerFrame;
import gui.FieldView;
import gui.GameOverFrame;
import gui.Parameter;
import items.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Game {
    // Attributes
    Parameter parameter;
    private ArrayList<Player> players;
    private String level;
    private String teamName;
    private String timeResult;
    ControllerFrame controllerFrame;
    FieldView fieldView;

    // Constructor
    public Game(String level) {
        this.level = level;
        this.parameter = new Parameter();
        this.players = new ArrayList<Player>();
        fieldView = new FieldView(parameter);
        controllerFrame = new ControllerFrame(parameter, this);
    }

    // Methods
    public void startGame() throws IOException, InterruptedException {
        readDataFromFile();
        fieldView.draw();
        controllerFrame.instantianateFrame();
        fieldView.getFrame().setLocation(50,50);
        controllerFrame.getMainFrame().setLocation(750,250);
        this.parameter.setControllerFrame(controllerFrame);
    }

    public void readDataFromFile() throws IOException {
        parameter.setDifficultyLevel(this.level);
        File file;
        System.out.println(level);
        if (this.level != "") {
            file = new File("src/files/" + this.level + ".txt");
        } else {
            file = new File("src/files/Easy.txt");
        }
        BufferedReader br = new BufferedReader(new FileReader(file));
        String str;
        while ((str = br.readLine()) != null) {
            String[] arrOfStr = str.split(" ", 2);
            System.out.println(arrOfStr[0] + " " + arrOfStr[1]);
            switch (arrOfStr[0]) {
                case "Iceberg:":
                    int number_of_icebergs = Integer.parseInt(arrOfStr[1].trim());
                    this.parameter.setNumber_of_icebergs(number_of_icebergs);
                    for (int i = 0; i < number_of_icebergs; i++) {
                        Iceberg iceberg = new Iceberg();
                        iceberg.setID(i + 1);
                        setIcebergParameters(iceberg);
                        this.parameter.addIcebergs(iceberg);
                    }
                    setIcebergNeighbours();
                    break;
                case "Eskimos:":
                    int number_of_eskimos = Integer.parseInt(arrOfStr[1].trim());
                    this.parameter.setNumber_of_eskimos(number_of_eskimos);
                    for (int i = 0; i < number_of_eskimos; i++) {
                        Eskimos eskimos = new Eskimos();
                        eskimos.setBodyHeat(5);
                        eskimos.setFallen(false);
                        eskimos.setHasIgloo(false);
                        eskimos.setID(i + 1);
                        placeEskimosOnIceberg(eskimos);
                        this.parameter.addEskimo(eskimos);
                    }
                    break;
                case "Explorer:":
                    int number_of_explorers = Integer.parseInt(arrOfStr[1].trim());
                    this.parameter.setNumber_of_explorers(number_of_explorers);
                    for (int i = 0; i < number_of_explorers; i++) {
                        Explorer explorer = new Explorer();
                        explorer.setBodyHeat(4);
                        explorer.setFallen(false);
                        explorer.setID(i + 1);
                        placeExplorerOnIceberg(explorer);
                        this.parameter.addExplorer(explorer);
                    }
                    break;
                case "Food:":
                    int number_of_food = Integer.parseInt(arrOfStr[1].trim());
                    this.parameter.setNumber_of_food(number_of_food);
                    for (int i = 0; i < number_of_food; i++) {
                        Food food = new Food();
                        this.parameter.addFood(food);
                        placeFoodOnIceberg(food);
                    }
                    break;
                case "DivingSuit:":
                    int number_of_divingSuit = Integer.parseInt(arrOfStr[1].trim());
                    this.parameter.setNumber_of_divingsuits(number_of_divingSuit);
                    System.out.println(number_of_divingSuit);
                    for (int i = 0; i < number_of_divingSuit; i++) {
                        DivingSuit divingSuit = new DivingSuit();
                        this.parameter.addDivingSuit(divingSuit);
                        placeDivingSuitOnIceberg(divingSuit);
                    }
                    break;
                case "Rope:":
                    int number_of_rope = Integer.parseInt(arrOfStr[1].trim());
                    this.parameter.setNumber_of_ropes(number_of_rope);
                    for (int i = 0; i < number_of_rope; i++) {
                        Rope rope = new Rope();
                        this.parameter.addRope(rope);
                        placeRopesOnIceberg(rope);
                    }
                    break;
                case "Shovel:":
                    int number_of_shovel = Integer.parseInt(arrOfStr[1].trim());
                    this.parameter.setNumber_of_shovels(number_of_shovel);
                    for (int i = 0; i < number_of_shovel; i++) {
                        Shovel shovel = new Shovel();
                        this.parameter.addShovel(shovel);
                        placeShovelsOnIceberg(shovel);
                    }
                    break;
                case "Hole:":
                    int number_of_hole = Integer.parseInt(arrOfStr[1].trim());
                    this.parameter.setNumber_of_holes(number_of_hole);
                    Hole hole1 = new Hole();
                    Hole hole2 = new Hole();
                    break;
                case "FlareGun:":
                    for (int i = 0; i < 3; i++) {
                        FlareGun flareGun = new FlareGun();
                        placeFlareGunOnIceberg(flareGun);
                    }
                    break;
            }
        }
    }


    public void setIcebergParameters(Iceberg iceberg) {
        if (generateRandomNumber(2) == 0) {
            iceberg.setCapacity(generateRandomNumber(3) + 1);
            iceberg.setStable(false);
        } else if (generateRandomNumber(2) == 1) {
            iceberg.setCapacity(Integer.MAX_VALUE);
            iceberg.setStable(true);
        }
        iceberg.setAmountOfSnow(generateRandomNumber(3) + 1);
        iceberg.setHasIgloo(false);
    }

    public void setIcebergNeighbours() {
        switch (this.parameter.getNumber_of_icebergs()) {
            case 16:
                for (int i = 0; i < this.parameter.getIcebergs().size(); i++) {
                    if ((i % 4) - 1 >= 0)
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i - 1));
                    if ((i % 4) + 1 <= 3)
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i + 1));
                    if (i <= 11)
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i + 4));
                    if (i >= 4 && i <= 15) {
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i - 4));
                    }
                }
                break;
            case 25:
                for (int i = 0; i < this.parameter.getIcebergs().size(); i++) {
                    if ((i % 5) - 1 >= 0)
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i - 1));
                    if ((i % 5) + 1 <= 4)
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i + 1));
                    if (i <= 19)
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i + 5));
                    if (i >= 5 && i <= 24) {
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i - 5));
                    }
                }
                break;
            case 36:
                for (int i = 0; i < this.parameter.getIcebergs().size(); i++) {
                    if ((i % 6) - 1 >= 0)
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i - 1));
                    if ((i % 6) + 1 <= 5)
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i + 1));
                    if (i <= 29)
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i + 6));
                    if (i >= 6 && i <= 35) {
                        this.parameter.getIcebergs().get(i).addNeighbour(this.parameter.getIcebergs().get(i - 6));
                    }
                }
                break;
        }
    }

    public void placeEskimosOnIceberg(Eskimos eskimos) {
        int randomID = 0;
        while (true) {
            randomID = generateRandomNumber(parameter.getNumber_of_icebergs());
            if (this.parameter.getIcebergs().get(randomID).getFigures().size() == 0) break;
            else continue;
        }
        this.parameter.getIcebergs().get(randomID).addEskimos(eskimos);
        eskimos.setOnIceberg(this.parameter.getIcebergs().get(randomID));
    }

    public void placeExplorerOnIceberg(Explorer explorer) {
        int randomID = 0;
        while (true) {
            randomID = generateRandomNumber(parameter.getNumber_of_icebergs());
            if (this.parameter.getIcebergs().get(randomID).getFigures().size() == 0) break;
            else continue;
        }
        this.parameter.getIcebergs().get(randomID).addExplorer(explorer);
        explorer.setOnIceberg(this.parameter.getIcebergs().get(randomID));
    }

    public void placeFoodOnIceberg(Food food) {
        int randomID = 0;
        while (true) {
            randomID = generateRandomNumber(parameter.getNumber_of_icebergs());
            if (this.parameter.getIcebergs().get(randomID).getItems().size() == 0) break;
            else continue;
        }
        this.parameter.getIcebergs().get(randomID).addFood(food);
    }

    public void placeDivingSuitOnIceberg(DivingSuit divingSuit) {
        int randomID = 0;
        while (true) {
            randomID = generateRandomNumber(parameter.getNumber_of_icebergs());
            if (this.parameter.getIcebergs().get(randomID).getItems().size() == 0) break;
            else continue;
        }
        this.parameter.getIcebergs().get(randomID).addDivingSuit(divingSuit);
    }

    public void placeRopesOnIceberg(Rope rope) {
        int randomID = 0;
        while (true) {
            randomID = generateRandomNumber(parameter.getNumber_of_icebergs());
            if (this.parameter.getIcebergs().get(randomID).getItems().size() == 0) break;
            else continue;
        }
        this.parameter.getIcebergs().get(randomID).addRope(rope);
    }

    public void placeShovelsOnIceberg(Shovel shovel) {
        int randomID = 0;
        while (true) {
            randomID = generateRandomNumber(parameter.getNumber_of_icebergs());
            if (this.parameter.getIcebergs().get(randomID).getItems().size() == 0) break;
            else continue;
        }
        this.parameter.getIcebergs().get(randomID).addShovel(shovel);
    }

    public void placeFlareGunOnIceberg (FlareGun flareGun) {
        int randomID = 0;
        while (true) {
            randomID = generateRandomNumber(parameter.getNumber_of_icebergs());
            if (this.parameter.getIcebergs().get(randomID).getItems().size() == 0) break;
            else continue;
        }
        this.parameter.getIcebergs().get(randomID).addFlareGun(flareGun);
    }

    public int generateRandomNumber(int level) {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(level);
        return randomInt;
    }

    public void winGame(String result){
        GameOverFrame winGame = new GameOverFrame(result);
        winGame.instantiateFrame(true);
    }

    public void loseGame(String result){
        GameOverFrame winGame = new GameOverFrame(result);
        winGame.instantiateFrame(false);
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public void addPlayer(Player player){
        this.players.add(player);
    }

    public Parameter getParameter() {
        return parameter;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public String getLevel() {
        return level;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getTimeResult() {
        return timeResult;
    }

    public FieldView getFieldView() {
        return fieldView;
    }
}
