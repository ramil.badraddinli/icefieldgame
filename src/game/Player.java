package game;

public class Player {
    private String userName;
    private int remainedMoves;

    public Player(String userName) {
        this.userName = userName;
        remainedMoves = 4;
    }

    public String getUserName() {
        return userName;
    }

    public int getRemainedMoves() {
        return remainedMoves;
    }
}
