package figures;

import elements.Iceberg;

/**
 * Represents explorer type of figure class
 */
public class Explorer extends Figure{
    /**
     * An iceberg object representing the iceberg of which explorer has checked the capacity
     */
    private Iceberg checkedIceberg;

    /** Constructor
     *
     * @param bodyHeat body heat of explorer
     * @param isFallen whether it has fallen into water or not
     * @param onIceberg iceberg object is to be set to the explorer in which it will be placed on
     * @param ID Integer representing ID of eskimo
     */
    public Explorer(int bodyHeat, boolean isFallen, Iceberg onIceberg, int ID) {
        super(bodyHeat, isFallen, onIceberg, ID);
        checkedIceberg = null;
    }

    /** Constructor
     *
     */
    public Explorer() {
        super();
        checkedIceberg= null;
    }

    /** Gives the iceberg which explorer has checked
     *
     * @return iceberg object
     */
    public Iceberg getCheckedIceberg() {
        return checkedIceberg;
    }

    /** Sets the checkedIceberg field
     *
     * @param checkedIceberg iceberg object
     */
    public void setCheckedIceberg(Iceberg checkedIceberg) {
        this.checkedIceberg = checkedIceberg;
    }

    /** Checks the capacity of iceberg
     *
     * @param iceberg iceberg object whose capacity is to be checked
     * @return integer representing ID of checked iceberg
     */
    public int checkCapacity(Iceberg iceberg){
        setCheckedIceberg(iceberg);
        return iceberg.getID();
    }
}
