package figures;

import elements.Iceberg;
import items.*;

import java.util.ArrayList;

/**
 * Represents Figure class
 */
public class Figure {
    /**
     * Integer indicates ID of figure
     */
    protected int ID;

    /**
     * Represents the body heat of figure
     */
    protected int bodyHeat;

    /**
     * Represents whether figure has fallen into water or not
     */
    protected boolean isFallen;

    /**
     * Represents iceberg object that figure is placed on
     */
    protected Iceberg onIceberg;

    protected ArrayList<Item> items;

    /**
     * Constructor for Figure objects
     *
     * @param bodyHeat  body of heat figure
     * @param isFallen  whether it has fallen into water or not
     * @param onIceberg iceberg object is to be set to the figure in which it will be placed on
     * @param ID        Integer ID of figure
     */
    public Figure(int bodyHeat, boolean isFallen, Iceberg onIceberg, int ID) {
        this.bodyHeat = bodyHeat;
        this.isFallen = isFallen;
        this.onIceberg = onIceberg;
        this.ID = ID;
        this.items = new ArrayList<>();
    }

    /**
     * Constructor for Figure objects
     */
    public Figure() {
        this.bodyHeat = 1;
        this.isFallen = false;
        this.onIceberg = null;
        this.items = new ArrayList<>();
    }


    /**
     * Gets the ID of figure
     *
     * @return an integer representing ID of figure
     */
    public int getID() {
        return ID;
    }

    /**
     * Sets the ID of figure
     *
     * @param ID integer which is to be set for ID field
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * Gets body heat of figure
     *
     * @return integer representing bodyHeat of figure
     */
    public int getBodyHeat() {
        return bodyHeat;
    }

    /**
     * Sets body heat of figure
     *
     * @param bodyHeat integer which is to set for bodyHeat field
     */
    public void setBodyHeat(int bodyHeat) {
        this.bodyHeat = bodyHeat;
    }

    /**
     * Checks whether figure has fallen into water or not
     *
     * @return boolean value representing if figure has fallen into water or not
     */
    public boolean isFallen() {
        return isFallen;
    }

    /**
     * Sets isFallen field
     *
     * @param fallen boolean value which is to be set for isFallen field
     */
    public void setFallen(boolean fallen) {
        isFallen = fallen;
    }

    /**
     * Returns iceberg object on which figure stands on
     *
     * @return iceberg object
     */
    public Iceberg getOnIceberg() {
        return onIceberg;
    }

    /**
     * Sets onIceberg field
     *
     * @param onIceberg iceberg object which is to be set for onIceberg field
     */
    public void setOnIceberg(Iceberg onIceberg) {
        this.onIceberg = onIceberg;
    }

    /**
     * Increases body heat of figure by one unit
     *
     * @param unit integer representing how much body heat is increased
     */
    public void increaseBodyHeat(int unit) {
        this.bodyHeat += unit;
    }

    public boolean addItem(Item item) {
        return this.items.add(item);
    }

    public void removeItem(String type) {
        int i = 0;
        switch (type) {
            case "Food":
                while (!(items.get(i) instanceof Food)) i++;
                if (i < items.size()) items.remove(i);
            case "Shovel":
                while (!(items.get(i) instanceof Shovel)) i++;
                if (i < items.size()) items.remove(i);
            case "DivingSuit":
                while (!(items.get(i) instanceof DivingSuit)) i++;
                if (i < items.size()) items.remove(i);
            case "Rope":
                while (!(items.get(i) instanceof Rope)) i++;
                if (i < items.size()) items.remove(i);
        }
    }

    public boolean hasItem(String type) {
        int i = 0;
        switch (type) {
            case "Food":
                while (!(items.get(i) instanceof Food)) i++;
            case "Shovel":
                while (!(items.get(i) instanceof Shovel)) i++;
            case "DivingSuit":
                while (!(items.get(i) instanceof DivingSuit)) i++;
            case "Rope":
                while (!(items.get(i) instanceof Rope)) i++;
        }
        return (i < items.size());
    }

    public ArrayList<Item> getItems() {
        return this.items;
    }
}
