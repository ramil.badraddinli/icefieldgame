package figures;

import elements.Iceberg;

/**
 * Represents eskimo type of figure class
 */
public class Eskimos extends Figure{
    /**
     * Boolean value representing whether eskimo has an igloo or not
     */
    private boolean hasIgloo;

    /** Constructor
     *
     * @param bodyHeat body heat of eskimo
     * @param isFallen whether it has fallen into water or not
     * @param onIceberg iceberg object is to be set to the eskimo in which it will be placed on
     * @param hasIgloo value is to be set that representing whether eskimo has an igloo or not
     * @param ID Integer representing ID of eskimo
     */
    public Eskimos(int bodyHeat, boolean isFallen, Iceberg onIceberg, boolean hasIgloo, int ID) {
        super(bodyHeat, isFallen, onIceberg, ID);
        this.hasIgloo = hasIgloo;
    }

    /** Constructor
     *
     */
    public Eskimos() {
        super();
        this.hasIgloo = false;
    }

    /** Gets current status of hasIgloo field(true or false)
     *
     * @return true if eskimo has an igloo, otherwise false
     */
    public boolean getHasIgloo() {
        return hasIgloo;
    }

    /** Sets the status of hasIgloo field
     *
     * @param hasIgloo boolean value is to be set for hasIgloo field
     */
    public void setHasIgloo(boolean hasIgloo) {
        this.hasIgloo = hasIgloo;
    }

    /** Builds an igloo for eskimo and returns integer representing ID of iceberg which an igloo has been built on
     *
     * @param iceberg represents iceberg object where igloo is built
     * @return integer which is ID of iceberg
     */
    public int buildIgloo(Iceberg iceberg){
        hasIgloo = true;
        iceberg.setHasIgloo(true);
        return iceberg.getID();
    }
}
