package moves;

import elements.Iceberg;
import figures.Figure;

/**
 * Represents movement from one iceberg to another
 */
public class StepTo implements  IMove{
    /** Moving to another iceberg functionality
     *
     * @param figure represents figure who will move to iceberg
     * @param iceberg represent iceberg to which figure will move to
     * @return boolean value whether movement was successful or not
     */
    public boolean moveTo(Figure figure, Iceberg iceberg){
        if(figure==null || iceberg==null) return false;
        Iceberg previousIceberg = figure.getOnIceberg();
        previousIceberg.removeFigure(figure);
        if(iceberg.isStable()==false || iceberg.getCapacity()-iceberg.getFigures().size() <= 0)
            return  false;
        iceberg.addFigure(figure);
        figure.setOnIceberg(iceberg);
        return true;
    }
}
