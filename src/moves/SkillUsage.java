package moves;

import elements.Iceberg;
import figures.Eskimos;
import figures.Explorer;
import figures.Figure;

/**
 * Represents skill usage
 */
public class SkillUsage implements IMove{
    /** Applies each type of figure's skill
     *
     * @param figure represents figure who will use skill depend on its type
     * @param iceberg represents iceberg object in which skill will be applied by figure
     * @return return integer representing skill is applied successfully
     */
    public int applySkill(Figure figure, Iceberg iceberg){
        if (figure instanceof Eskimos){
            return ((Eskimos) figure).buildIgloo(iceberg);
        }
        else if(figure instanceof Explorer){
            return ((Explorer) figure).checkCapacity(iceberg);
        }
        return -1;
    }
}
