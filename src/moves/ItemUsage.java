package moves;

import elements.Iceberg;
import items.Item;

/**
 * Represents item usage
 */
public class ItemUsage implements  IMove{
    /** Provides item usage functionality
     *
     * @param item represents item object
     * @param object an object
     * @return true if item is used correctly, otherwise false
     */
    public boolean useItem(Item item, Object object){
        if(item == null) return false;
        return item.useItem(object);
    }

    /** Retrieves item from specified iceberg
     *
     * @param iceberg represents iceberg object where items are retrieved from
     * @return true if item is retrieved correctly, otherwise false
     */
    public boolean retrieveItem(Iceberg iceberg){
        if(iceberg==null) return false;
        if(iceberg.isStable()==false || iceberg.getAmountOfSnow()!=0) return false;
        if(iceberg.getAmountOfSnow()==0){
            //retrieve item
            return true;
        }
        return false;
    }
}
